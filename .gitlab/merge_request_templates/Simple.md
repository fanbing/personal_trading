## Checklist & Code Review Process

- Did you lint your code using **pylint** and get a score above 5?
- Did you **document/comment your code** using the proper doc style?
- Are you **rebased** and exactly one commit above master?
- Did you **run and pass all the tests** as expected?
- Did you fill out the **PR template** and provide proper commit messages?