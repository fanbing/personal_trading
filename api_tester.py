from api.exchange_api import ExchangeAPI
# from prettyprinter import cpprint
import yaml


class APItester:
    def __init__(self):
        with open('config.yaml', 'r') as config_file:
            # init
            data = (yaml.load(config_file))
            self.exchange_list = ['HUOBI', 'HADAX', 'KUCOIN',
                                  'BITTREX', 'GATEIO', 'BINANCE']
            self.credentials = {}
            for exchange in self.exchange_list:
                try:
                    self.credentials[exchange] = data['CREDENTIALS'][exchange]
                except KeyError:
                    print('%s has no credentials stored in the config file.'
                          % exchange)
        self.api = self.create_connection()

    def create_connection(self):
        print(self.exchange_list)
        target_exchange = input('Please select one of the above '
                                'exchanges as your test target: ').upper()
        algo_info = {'exchange_name': target_exchange,
                     'algo_name': 'FUNCTIONAL_TEST'}
        credentials = self.credentials[target_exchange]
        return ExchangeAPI('not_logged_test_session', algo_info, credentials)


def main():
    at = APItester()
    api = at.create_connection()
    print("Pricision Points:")
    print(api.get_precision('ETH', 'BTC'))
    print("\nOrderBook:")
    print(api.get_orderbook('ETH', 'BTC', 10))
    print("\nKline: ")
    print(api.get_kline('ETH', 'BTC', '1d', 10))
    print("\nMarket Trade History:")
    print(api.get_market_trade_history('ETH', 'BTC', 5))
    print("\nBalance:")
    print(api.get_balance('ETH'))
    print("\nOpen Orders:")
    print(api.get_open_order('ETH', 'BTC'))
    print("\nPlace Order:")
    print(api.place_order('ETH', 'BTC', 'SELL', 1.234, 1))
    print("\nCancel Order:")
    print(api.cancel_order('ETH', 'BTC', '1234', 'SELL'))
    
#main()
