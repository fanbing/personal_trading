"""
This script is used to send a notification on trading-events channel of slack.
"""
from urllib import request
import json

def send_message_to_slack(webhook_url, text):
    """
    This function takes the url request from the slack incoming webhooks.
    And the message is passed to the function for the notification on trading-events channel on slack.
    """
    post = {"text": "{0}".format('`%s`' % text)}
    try:
        json_data = json.dumps(post)
        req = request.Request(webhook_url,    # URL for channel you want to have message posted   
                              data=json_data.encode('ascii'),
                              headers={'Content-Type': 'application/json'})
        request.urlopen(req)
        return True
    except Exception as em:
        print("EXCEPTION: " + str(em))
        return False
