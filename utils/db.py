"""Database Connections File"""
from pymongo import MongoClient
import time
import datetime
# import utils.logging

class Database:
    """ Database Class to manage all db connections"""
    def __init__(self, data, credentials):
        self.username = credentials['USERNAME']
        self.password = credentials['PASSWORD']
        self.url = credentials['URL']
        self.db_name = credentials['DB_NAME']
        self.port = str(credentials['PORT_NUMBER'])
        self.config_info = data
        self.db = None

    def get_db(self):
        uri = "mongodb://" \
              + self.username + ":" + self.password \
              + "@" \
              + self.url + ":" + self.port \
              + "/" + self.db_name
        try:
            client = MongoClient(uri,
                                 connectTimeoutMS=30000,
                                 socketTimeoutMS=None,
                                 socketKeepAlive=True)

            db = client.get_default_database()
            print(db)
            return db
        except Exception as e:
            print(e)

    def create_session(self):
        algo_name = self.config_info['CHOSEN_ALGO']
        start_time = int(time.time())
        end_time = start_time + self.config_info[algo_name]['TIME_BOUND']
        str_start_time = datetime.datetime.fromtimestamp(start_time).strftime('%Y-%m-%d %H:%M:%S')
        str_end_time = datetime.datetime.fromtimestamp(end_time).strftime('%Y-%m-%d %H:%M:%S')
        session_content = {
            'algo_name': algo_name,
            'client_name': self.config_info['CLIENT_NAME'],
            'trader_name': self.config_info['TRADER'],
            'book_name': self.config_info['BOOK_NAME'],
            'start_time': str_start_time,
            'end_time': str_end_time,
            'config_info': self.config_info[algo_name]
        }
        print(session_content)
        db = self.get_db()
        trade_session_logger = db['bot_trade_session']
        session_id = trade_session_logger.insert_one(session_content)
        # load config file
        # calculate stuff
        # create object
        # insert one
        # get return session id
        return session_id.inserted_id