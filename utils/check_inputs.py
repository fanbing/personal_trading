from prettyprinter import cpprint
from api.exchange_api import ExchangeAPI


def check_iceberg_inputs(data, session_id):
    """Iceberg Inputs"""
    algo_inputs = data["ICEBERG"]
    exchange_name = data["ICEBERG"]["EXCHANGE_NAME"]
    exchange_credentials = data["CREDENTIALS"][exchange_name]
    algo_info = {'exchange_name': exchange_name, 
    'algo_name': 'iceberg'}
    api = ExchangeAPI(session_id, algo_info, exchange_credentials)
    token_name = data["ICEBERG"]["QUOTE_CURRENCY"]
    base_name = data["ICEBERG"]["BASE_CURRENCY"]
    current_token_balance = api.get_balance(token_name)
    current_base_balance = api.get_balance(base_name)
    print("-----Current algo inputs: -----")
    cpprint(algo_inputs)
    print("CURRENT TOKEN BALANCE:  " + str(current_token_balance))
    print("CURRENT BASE BALANCE:  " + str(current_base_balance))
    # show balance from Exchange API here
    # del the api object after
    confirmed = input("To start trading, type 'Y'. To exit, type anything other than 'Y': ")
    if confirmed == "Y":
        return True
    print("Inputs not confirmed. Exiting.")
    return False

def check_twap_inputs(data, session_id):
    """Iceberg Inputs"""
    algo_inputs = data["TWAP"]
    exchange_name = data["TWAP"]["EXCHANGE_NAME"]
    exchange_credentials = data["CREDENTIALS"][exchange_name]
    algo_info = {'exchange_name': exchange_name,  
    'algo_name': 'twap'}
    api = ExchangeAPI(session_id, algo_info, exchange_credentials)
    quote_currency = data["TWAP"]["QUOTE_CURRENCY"]
    base_currency = data["TWAP"]["BASE_CURRENCY"]
    quote_currency_balance = api.get_balance(quote_currency)
    base_currency_balance = api.get_balance(base_currency)
    print("-----Current algo inputs: -----")
    cpprint(algo_inputs)
    print("CURRENT QUOTE CURRENCY BALANCE:  " + str(quote_currency_balance))
    print("CURRENT BASE CURRENCY BALANCE:  " + str(base_currency_balance))
    confirmed = input("To start trading, type 'Y'. To exit, type anything other than 'Y': ")
    # show balance from Exchange API here
    # del the api object after
    if confirmed == "Y":
        return True
    print("Inputs not confirmed. Exiting.")
    return False

def check_dynamic_wall_inputs(data, session_id):
    """Dynamic_wall Inputs"""
    algo_inputs = data["DYNAMIC_WALL"]
    exchange_name = data["DYNAMIC_WALL"]["EXCHANGE_NAME"]
    exchange_credentials = data["CREDENTIALS"][exchange_name]
    algo_info = {'exchange_name': exchange_name, 
    'algo_name': 'DYNAMIC_WALL'}
    api = ExchangeAPI(session_id, algo_info, exchange_credentials)
    quote_currency = data["DYNAMIC_WALL"]["QUOTE_CURRENCY"]
    base_currency = data["DYNAMIC_WALL"]["BASE_CURRENCY"]
    quote_currency_balance = api.get_balance(quote_currency)
    base_currency_balance = api.get_balance(base_currency)
    print("-----Current algo inputs: -----")
    cpprint(algo_inputs)
    print("CURRENT QUOTE CURRENCY BALANCE:  " + str(quote_currency_balance))
    print("CURRENT BASE CURRENCY BALANCE:  " + str(base_currency_balance))
    # show balance from Exchange API here
    # del the api object after
    confirmed = input("To start trading, type 'Y'. To exit, type anything other than 'Y': ")
    if confirmed == "Y":
        return True
    print("Inputs not confirmed. Exiting.")
    return False

def check_vexplode_inputs(data, session_id):
    """Dynamic_wall Inputs"""
    algo_inputs = data["VEXPLODE"]
    print("-----Current algo inputs: -----")
    cpprint(algo_inputs)
    # show balance from Exchange API here
    # del the api object after
    confirmed = input("To start trading, type 'Y'. To exit, type anything other than 'Y': ")
    if confirmed == "Y":
        return True
    print("Inputs not confirmed. Exiting.")
    return False