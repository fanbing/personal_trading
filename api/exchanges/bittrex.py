from bittrex.bittrex import Bittrex, API_V2_0
import time

class BittrexAPI():
    def __init__(self, api_key, api_secret):
        """
        Bittrex is currently restricting orders to 500 open orders 
        and 200,000 orders a day. If the user is affected by these 
        limits as an active trader, please visit https://support.bittrex.com 
        and open a support ticket.

        This package is based on another wraper: 
        https://github.com/ericsomdahl/python-bittrex/blob/master/bittrex/bittrex.py
        """
        self.api_key = api_key
        self.api_secret = api_secret
        self.api = Bittrex(self.api_key, self.api_secret)
        self.api2 = Bittrex(self.api_key, self.api_secret, api_version=API_V2_0)
        # According to official bittrex api, there is no function calling
        # the account id or it is not well defined. As a result, we will just
        # post the api_key as the account id (NOT the api_secret).
        self.ACCOUNT_ID = api_key

    def get_precision(self, base_currency, quote_currency):
        """
            Seems the precision for all currency pairs in Bittrex is 8.
        """
        return {
            'price_precision': 8,
            'quantity_precision_in_quote': 8
        }

    
    def get_orderbook(self, base_currency, quote_currency, limit):
        """
        limit here must be smaller than 100.
        """
        if limit>100:
            print('Limit surpassed for orderbook query.')
            return False
        market = base_currency.upper()+'-'+quote_currency.upper()
        try:
            response = self.api.get_orderbook(market)
            raw = response['result']
            orderbook_info = {'bids':[], 'asks':[]}
            for i in range(limit):
                orderbook_info['asks'].append({
                    'price': raw['sell'][i]['Rate'], 
                    'size_in_quote': raw['sell'][i]['Quantity'], 
                    'size_in_base': raw['sell'][i]['Rate']*raw['sell'][i]['Quantity']
                })
                orderbook_info['bids'].append({
                    'price': raw['buy'][i]['Rate'], 
                    'size_in_quote': raw['buy'][i]['Quantity'], 
                    'size_in_base': raw['buy'][i]['Rate']*raw['buy'][i]['Quantity']
                })
        except Exception as err:
            print('Get orderbook failed for Bittrex: %s ' % str(err))
            print('Message from exchange: %s', response['message'])
            return False
        return orderbook_info



    def get_kline(self, base_currency, quote_currency, granulation, limit):
        """
           This K-Line information utilise the 2.0 version of bittrex api.
           The granulation can only support one of five time intervals:
           60, 300, 1800, 3600, 86400 
        """
        if granulation == 3600:
            granulation = 'hour'
        elif granulation == 60:
            granulation = 'oneMin'
        elif granulation == 300:
            granulation = 'fiveMin'
        elif granulation == 1800:
            granulation = 'thirtyMin'
        elif granulation == 86400:
            granulation = 'day'
        else:
            print('Granulation can only be 60, 300, 1800, 3600 or 86400.')
            return False
        market = base_currency.upper()+'-'+quote_currency.upper()
        try:
            response = self.api2.get_candles(market, granulation)
            raw = response['result']
            raw.reverse()
            kline_info = []
            for i in range(limit):
                kline_info.append({
                    'open': raw[i]['O'],
                    'close': raw[i]['C'],
                    'high': raw[i]['H'],
                    'low': raw[i]['L'],
                    'volume_in_quote': raw[i]['V'],
                    'volume_in_base': raw[i]['BV']
                })
        except Exception as err:
            print('Get kline failed for Bittrex: %s ' % str(err))
            print('Message from exchange: %s', response['message'])
            return False
        return kline_info

    def str_to_timestamp(self, timestr):
        return int(time.mktime(time.strptime(timestr[:19], '%Y-%m-%dT%H:%M:%S')))

    def get_market_trade_history(self, base_currency, quote_currency, limit):
        """
            This function pulls out the trade history of a certain pair.
            Limit has an upper limit of 100.
        """
        if limit>100:
            print('Limit surpassed for orderbook query.')
            return False
        market = base_currency.upper()+'-'+quote_currency.upper()
        try:
            response = self.api.get_market_history(market)
            raw = response['result']
            market_history = []
            for i in range(limit):
                market_history.append({
                    'price': raw[i]['Price'],
                    'size_in_quote': raw[i]['Quantity'],
                    'order_type': raw[i]['OrderType'].lower(),
                    'timestamp': self.str_to_timestamp(raw[i]['TimeStamp'])
                })
        except Exception as err:
            print('Get trade history failed for Bittrex: %s ' % str(err))
            print('Message from exchange: %s', response['message'])
            return False
        return market_history


    def get_balance(self, coin_name):
        try:
            response = self.api.get_balance(coin_name.upper())
            raw = response['result']
            balance_info = {}
            balance_info['available_balance'] = raw['Available']
            if balance_info['available_balance'] == None:
                balance_info['available_balance'] = 0
            balance_info['reserved_balance'] = raw['Pending']
            if balance_info['reserved_balance'] == None:
                balance_info['reserved_balance'] = 0
        except Exception as err:
            print('Get balance failed for Bittrex: %s ' % str(err))
            print('Message from exchange: %s', response['message'])
            return False
        return balance_info

    def get_open_order(self, base_currency, quote_currency):
        market = base_currency.upper()+'-'+quote_currency.upper()
        try:
            response = self.api.get_open_orders(market)
            raw = response['result']
            open_order_list = []
            for order in raw:
                open_order_list.append({
                    'order_id': order['OrderUuid'],
                    'order_type': order['OrderType'].lower().replace('limit_', ''),
                    'filled_quote': order['Quantity']-order['QuantityRemaining'],
                    'price': order['Limit']
                })
        except Exception as err:
            print('Get open orders failed for Bittrex: %s ' % str(err))
            print('Message from exchange: %s', response['message'])
            return False
        return open_order_list

    def place_order(self, base_currency, quote_currency, order_type, price, size_in_quote_currency):
        """
            Tested on Aug-08
        """
        market = base_currency.upper()+'-'+quote_currency.upper()
        try:
            if order_type == 'buy':
                response = self.api.buy_limit(market, size_in_quote_currency, price)
                raw = response['result']
            elif order_type == 'sell':
                response = self.api.sell_limit(market, size_in_quote_currency, price)
                raw = response['result']
            order_id = str(raw['uuid'])
        except Exception as err:
            print('Place %s order failed for Bittrex: %s ' % (order_type, str(err)))
            print('Message from exchange: %s', response['message'])
            return False
        return order_id

    def cancel_order(self, base_currency, quote_currency, order_id, order_type):
        try:
            response = self.api.cancel(order_id)
        except Exception as err:
            print('Cancel order %s failed for Bittrex: %s ' % (order_id, str(err)))
            print('Message from exchange: %s', response['message'])
            return False
        return response['success']

    def get_order_details(self, base_currency, quote_currency, order_id, order_type):
        """
            If in the return data, price is equal to 0, it means that
            the order has not been filled.
            In bittrex, when you are selling, the fee is charged in the currency you receive. So 
            effectively if you are selling 100 CBC, the BTC you will get is 0.9975*100*price.
            When you are buying, the fee is also charged in base_currency. In another word, you will
            need to pay 100.25*price amount of BTC to buy 100 CBC. As a result, we cannot buy btc_balance/price 
            amount of CBC as we will have no money to pay fee.
        """
        try:
            response = self.api.get_order(order_id)
            raw = response['result']
            if raw['Price'] == 0:
                status = 'unfilled'
            else:
                if not raw['IsOpen']:
                    status = 'filled'
                else:
                    status = 'partial'
            order_detail = {
                    'status' : status,
                    'timestamp': self.str_to_timestamp(raw['Opened']),
                    'order_id': raw['OrderUuid'],
                    'order_type': raw['Type'].lower().replace('limit_', ''),
                    'price': raw['PricePerUnit'],
                    'filled_quantity_in_quote': raw['Quantity'] - raw['QuantityRemaining'],
                    'filled_quantity_in_base': (raw['Quantity'] - raw['QuantityRemaining'])*raw['Price'],
                    'fee': float(raw['CommissionPaid']),
                    'fee_unit': base_currency
                }
        except Exception as err:
            print('Get order details of %s failed for Bittrex: %s ' % (order_id, str(err)))
            print('Message from exchange: %s', response['message'])
            return False
        return order_detail
