import sys
import traceback
import base64
import hashlib
import hmac
import requests
import time
import configparser
from datetime import datetime

class KucoinAPI():
    def __init__(self, api_key, api_secret):
        self.API_PUB = api_key
        self.API_PRI = api_secret
        self.HOST = 'https://api.kucoin.com'
        self.ACCOUNT_ID = self.get_user_id()
            
            
    # Get time in milliseconds from current Date
    def dt_to_ms(self, dt):
        epoch = datetime.utcfromtimestamp(0)
        delta = dt - epoch
        return int(delta.total_seconds() * 1000)


    # Create Header for authentication
    def auth_header(self, endpoint, query_str):
        # Set current date time
        now = datetime.utcnow()
        nonce = str(self.dt_to_ms(now))
        str_for_sign = (endpoint + '/' + nonce + '/' + query_str).encode('utf-8')
        sign_result = hmac.new(self.API_PRI.encode('utf-8'), base64.b64encode(str_for_sign), hashlib.sha256)

        signature = sign_result.hexdigest()
        header = {
            'Accept': 'application/json',
            'KC-API-KEY': self.API_PUB,
            'KC-API-NONCE': nonce,
            'KC-API-SIGNATURE': signature
        }
        return header

    def get_precision(self, base_currency, quote_currency):
        quote_currency = quote_currency.upper()
        base_currency = base_currency.upper()
        if base_currency == 'ETH':
            price_precision = 7
        elif base_currency == 'BTC':
            price_precision = 8
        elif base_currency == 'USDT':
            price_precision = 6
        else:
            exit('Base Currency only supports ETH/BTC/USDT')

        endpoint = '/v1/market/open/coin-info?coin=' + quote_currency
        r = requests.get(self.HOST + endpoint)
        quantity_precision_in_quote = r.json()['data']['tradePrecision']
        return {
            'price_precision': int(price_precision),
            'quantity_precision_in_quote': int(quantity_precision_in_quote)
        }    

    def get_orderbook(self, base_currency, quote_currency, limit):
        """

        """
        symbol = quote_currency.upper() + '-' + base_currency.upper()
        endpoint = '/v1/open/orders?symbol=' +symbol+ '&limit=%s' % str(limit)
        r = requests.get(self.HOST + endpoint)   
        data = r.json()['data']
        dict = {'asks': [], 'bids': []}
        try:
            for ask in data['SELL']:
                dict['asks'].append({
                    'price': float(float(ask[0])),
                    'size_in_quote': float(float(ask[1])),
                    'size_in_base': float(float(ask[2]))
                })
            for bid in data['BUY']:
                dict['bids'].append({
                    'price': float(float(bid[0])),
                    'size_in_quote': float(float(bid[1])),
                    'size_in_base': float(float(bid[2]))
                })
            return dict
        except Exception as err:
            print("ORDER BOOK ERROR: " + str(err))
            print(traceback.format_exc())            
        

    def get_kline(self, base_currency, quote_currency, granulation, limit):
        symbol = quote_currency.upper() + '-' + base_currency.upper()
        t = requests.get(self.HOST + '/v1/open/markets')
        time_now = t.json()['timestamp']
        endpoint = '/v1/open/kline?symbol=' + symbol + '&type=' + str(granulation) + '&from=1500000000&to=' + str(time_now) + '&limit=' + str(limit)
        r = requests.get(self.HOST + endpoint)
        data = r.json()['data']
        list = []
        try:
            for chunk in data:
                temp = {}
                if chunk[1]:
                    temp['open'] = float(chunk[1])
                else:
                    temp['open'] = float(0)
                if chunk[4]:
                    temp['close'] = float(chunk[4])
                else:
                    temp['close'] = float(0)
                if chunk[3]:
                    temp['low'] = float(chunk[3])
                else:
                    temp['low'] = float(0)
                if chunk[2]:
                    temp['high'] = float(chunk[2])
                else:
                    temp['high'] = float(0)
                if chunk[5]:
                    temp['volume_in_quote'] = float(chunk[5])
                else:
                    temp['volume_in_quote'] = float(0)
                if chunk[6]:
                    temp['volume_in_base'] = float(chunk[6])
                else:
                    temp['volume_in_base'] = float(0)
                list.append(temp)    
            return list
        except Exception as err:
            print("KLINE ERROR: " + str(err))
            print(traceback.format_exc())     


    def get_market_trade_history(self, base_currency, quote_currency, limit):
        symbol = quote_currency.upper() + '-' + base_currency.upper()
        endpoint = '/v1/open/deal-orders?symbol=' + symbol + '&limit=' + str(limit)
        r = requests.get(self.HOST + endpoint) 
        result = r.json()['data']
        list = []
        try:
            for trade in result:
                if trade[1] == 'SELL': order_type = 'sell'
                else: order_type = 'buy'
                list.append({
                    'price': float(trade[2]),
                    'timestamp': int(trade[0]),
                    'size_in_quote': float(trade[3]),
                    'order_type': str(order_type)
                })
            return list
        except Exception as err:
            print('MARKET HISTORY ERROR: ' + str(err))
            print(traceback.format_exc())

    def get_user_id(self):
        endpoint = '/v1/user/info'
        query_str = ''
        for i in range(10):
            try:
                r = requests.get(self.HOST + endpoint, headers=self.auth_header(endpoint, query_str)) 
                return r.json()['data']['email']
            except KeyError:
                print('Failed to get account info due to Kucoin api issue. Wait 1 min to reboot.')
                time.sleep(60)
        print('Api failed to catch user info in 10 attempts. Please check your credentials and report to developer.')

    def get_balance(self, coin_name):
        symbol = coin_name.upper()
        endpoint = '/v1/account/' + symbol + '/balance'
        query_str = ''
        r = requests.get(self.HOST + endpoint, headers=self.auth_header(endpoint, query_str)) 
        return {
            'available_balance': float(r.json()['data']['balance']), 
            'reserved_balance': float(r.json()['data']['freezeBalance'])
        }


    def get_open_order(self, base_currency, quote_currency):
        symbol = quote_currency.upper() + '-' + base_currency.upper()
        endpoint = '/v1/order/active'
        data = {'symbol': symbol}
        query_str = 'symbol=' + str(symbol)
        r = requests.get(self.HOST + endpoint, params=data, headers=self.auth_header(endpoint, query_str))
        orders_info = []
        orders = r.json()['data']
        if orders:
            if orders['SELL']:
                for order in orders['SELL']:
                    orders_info.append({
                        'order_id': str(order[5]),
                        'order_type': str('sell'),
                        'price': float(order[2]),
                        'filled_quote': float(order[4])
                    })
            if orders['BUY']:
                for order in orders['BUY']:
                    orders_info.append({
                        'order_id': str(order[5]),
                        'order_type': str('buy'),
                        'price': float(order[2]),
                        'filled_quote': float(order[4])
                    })
            return orders_info


    def place_order(self, base_currency, quote_currency, order_type, price, size_in_quote_currency):
        symbol = quote_currency.upper() + '-' + base_currency.upper()
        precision = self.get_precision(base_currency, quote_currency)
        price = round(float(price), precision['price_precision'])
        if str(precision['quantity_precision_in_quote']) == '0':
            size_in_quote_currency = int(size_in_quote_currency)
        else:
            size_in_quote_currency = round(float(size_in_quote_currency), precision['quantity_precision_in_quote'])
        if order_type == 'buy':
            exchange_order_type = 'BUY'
        elif order_type == 'sell':
            exchange_order_type = 'SELL'
        else:
            exit('PLACE ORDER ERROR')

        endpoint = '/v1/'+symbol+'/order'
        data = {'type':exchange_order_type, 'amount':size_in_quote_currency, 'price':price}
        # !! query_str must in alphabetical order
        query_str = 'amount=' + str(size_in_quote_currency) + '&price=' + str(price) + '&type=' + exchange_order_type
        r = requests.post(self.HOST + endpoint, params=data, headers=self.auth_header(endpoint, query_str))
        return str(r.json()['data']['orderOid'])

    
    def cancel_order(self, base_currency, quote_currency, order_id, order_type):
        symbol = quote_currency.upper() + '-' + base_currency.upper()
        endpoint = '/v1/cancel-order'
        if order_type == 'buy':
            exchange_order_type = 'BUY'
        else:
            exchange_order_type = 'SELL'
        data = {'orderOid': order_id, 'symbol': symbol, 'type': exchange_order_type}
        query_str = 'orderOid=' + str(order_id) + '&symbol=' + str(symbol) + '&type=' + str(exchange_order_type)
        r = requests.post(self.HOST + endpoint, params=data, headers=self.auth_header(endpoint, query_str))
        if r.json()['success']:
            return True
        else:
            print('WARNING: Cancel_order_failed for %s!!!!!' % order_id)
            return False


    def get_order_details(self, base_currency, quote_currency, order_id, order_type):
        """
            Kucoin charges fee in terms of the currency you will get. Say you place an order to buy
            100 CBC, the result will be you used the same amount to buy 99.8 CBC.  This means if 
            you want to buy 100 CBC, you have to place an order to buy 100/0.998 CBC.
            When you are selling 100 CBC into BTC, the result is that you will only get 0.998*price*100
            amount of BTC. The returned data 'field-fee' has already accounted this change.
        """
        endpoint = '/v1/order/detail'
        fee = 0
        fee_rate = 0.001
        symbol = quote_currency.upper() + '-' + base_currency.upper()
        if order_type == 'buy':
            exchange_order_type = 'BUY'
        else:
            exchange_order_type = 'SELL'
        data = {'orderOid': order_id, 'symbol': symbol, 'type': exchange_order_type}
        query_str = 'orderOid=' + str(order_id) + '&symbol=' + str(symbol) + '&type=' + str(exchange_order_type)
        r = requests.get(self.HOST + endpoint, params=data, headers=self.auth_header(endpoint, query_str))
        result = r.json()['data']
        order_details = {
                'timestamp': int(r.json()['timestamp']),
                'order_id': str(order_id),
                'order_type': str(order_type),
                'price': float(result['dealPriceAverage']),
                'filled_quantity_in_quote': float(result['dealAmount']),
                'filled_quantity_in_base': float(result['dealValueTotal']),
            }
        if not result['isActive']:
            try:
                for trade in result['dealOrders']['datas']:
                    fee = fee+trade['fee']
                    fee_rate = trade['feeRate']
                if fee == 0:
                    order_details['status'] = 'unfilled'
                else:
                    order_details['status'] = 'filled'
            except:
                print('abnormal order %s marked as unfilled.' % order_id )
                order_details['status'] = 'unfilled'
        else:
            if result['dealAmount'] > 0:
                order_details['status'] = 'partial'
            else:
                order_details['status'] = 'unfilled'
            
        # determine if the status of the order
        if order_type == 'buy':
            order_details['fee_unit'] = quote_currency 
            order_details['fee'] = float(result['dealAmount'])*fee_rate
        elif order_type == 'sell':
            order_details['fee_unit'] = base_currency 
            order_details['fee'] = float(result['dealValueTotal'])*float(result['dealPriceAverage'])*fee_rate
        
        if result:
            return order_details
