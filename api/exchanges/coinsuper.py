"""
	coinsuper api intergration
	Date: 2018.07.19
"""
import hashlib
import time
import yaml
import requests


class CoinsuperAPI():

    def __init__(self, api_key, api_secret):
	    self.accesskey = api_key
	    self.secretkey = api_secret
	    self.ROOT_URL = 'https://api.coinsuper.com'
	    self.SUCCESS_CODES = [1000,1001]

    def get_request_data(self, params):
        # Pre-Encryption Params
        param_for_sign = params.copy()
        timestamp = int(time.time())
        param_for_sign["accesskey"] = self.accesskey
        param_for_sign["secretkey"] = self.secretkey
        param_for_sign["timestamp"] = timestamp
        sign = '&'.join(['{}={}'.format(k, param_for_sign[k])
                         for k in sorted(param_for_sign.keys())])
        # Generate Signature
        md5_str = hashlib.md5(sign.encode("utf8")).hexdigest()
        # Request Params
        request_data = {
            "common": {
                "accesskey": self.accesskey,
                "timestamp": timestamp,
                "sign": md5_str
            },
            "data": params
        }
        return request_data

    def http_post(self, resource, params):
        try:
            result = requests.post(self.ROOT_URL + resource, json=params, timeout=1)
            result = result.json()
            # the returned result is a string ||
            if int(result["code"]) not in self.SUCCESS_CODES:     
                print("\nERROR!")

            return result
        except:
            print("\nCoinSuper API failed.")

    # Get the entire balances of the account
    def get_precision(self, base_currency, quote_currency):   
        """
            There is no such data for precision, currently we are going to get
            it from the return value of orderbook info.
        """
        params = {"num": 1, 
				  "symbol": quote_currency.upper()+'/'+base_currency.upper()}
        result = self.http_post('/api/v1/market/orderBook',
                                self.get_request_data(params))['data']['result']
        price_sample = result['asks'][0]['limitPrice']
        quote_quantity_sample = result['asks'][0]['quantity']
        precision_info = {}
        precision_info['price_precision'] = len(price_sample.split('.')[1])
        precision_info['quantity_precision_in_quote'] = len(quote_quantity_sample.split('.')[1])
        return precision_info

    def get_orderbook(self, base_currency, quote_currency, limit):
        params = {"num": limit, 
				  "symbol": quote_currency.upper()+'/'+base_currency.upper()}
        result = self.http_post('/api/v1/market/orderBook',
                                self.get_request_data(params))
        # return only the dictionary of asks and bids.
        # asks and bids are lists of dictionaries.
        order_book = {'asks':[], 'bids':[]}
        # print(order_book)
        for ask_order in result["data"]["result"]['asks']:
       		order_book['asks'].append(
				{'price': float(ask_order['limitPrice']), 
				'size_in_quote': float(ask_order['quantity']), 
				'size_in_base': float(ask_order['limitPrice'])*float(ask_order['quantity'])})

        for bid_order in result["data"]["result"]['bids']:
            order_book['bids'].append(
				{'price': float(bid_order['limitPrice']), 
				'size_in_quote': float(bid_order['quantity']), 
				'size_in_base': float(bid_order['limitPrice'])*float(bid_order['quantity'])})
        return order_book
            
    def get_kline(self, base_currency, quote_currency, granulation, limit):
        """
		!!! For coinsuper, only 5 minutes kline information is available !!!
		"""
        params = {'symbol': quote_currency.upper()+'/'+base_currency.upper(), 'num': limit}
        result = self.http_post('/api/v1/market/kline', self.get_request_data(params))
        kline_info = []
        for kline in result["data"]["result"]:
            kline_item = {
                'open': float(kline['open']),
                'close': float(kline['close']),
                'high': float(kline['high']),
                'low': float(kline['low']),
                'volume_in_quote': float(kline['volume']),
                'volume_in_base': float(0)
            }
            kline_info.append(kline_item)
        return kline_info  

    def get_market_trade_history(self, base_currency, quote_currency, limit):
        """
			For coinsuper, limit must smaller than 50!!!
		"""
        params = {'symbol': quote_currency.upper()+'/'+base_currency.upper()}
        result = self.http_post('/api/v1/market/tickers', self.get_request_data(params))
        trades = []
        for trade in result["data"]["result"][:limit]:
            trades.append({
                'price': float(trade['price']),
                'timestamp': int(trade['timestamp']/1000),
                'size_in_quote': float(trade['volume']),
                'order_type': trade['tradeType'].lower()
            })
        return trades

    def get_balance(self, coin_name):
        params = {}
        result = self.http_post('/api/v1/asset/userAssetInfo', self.get_request_data(params))
        result = result["data"]["result"]['asset']
        balance_info = {}
        balance_info['available_balance'] = float(result[coin_name.upper()]['available'])
        balance_info['reserved_balance'] = float(result[coin_name.upper()]['total'])-float(result[coin_name.upper()]['available'])
        return balance_info

    def get_open_order(self, base_currency, quote_currency):
        params = {
			'symbol': quote_currency.upper()+'/'+base_currency.upper(),
			'num': 1000
		}
        result = self.http_post('/api/v1/order/openList', self.get_request_data(params))
        order_list = result['data']['result']
        orders_info = []
        for order_id in order_list:
            order_info = {}
            res = self.get_order_details(base_currency, quote_currency, order_id)
            order_info['order_id'] = res['order_id']
            order_info['order_type'] = res['order_type']
            order_info['price'] = res['price']
            order_info['filled_quote'] = res['filled_quantity_in_quote']
            orders_info.append(order_info)
        return orders_info

    def place_order(self, base_currency, quote_currency, 
					order_type, price, size_in_quote_currency):
        params = {
			'symbol': quote_currency.upper()+'/'+base_currency.upper(),
			'priceLimit': price,
			'orderType': 'LMT',
			'quantity': size_in_quote_currency,
			'amount': 0
		}
        if order_type == 'buy':
            result = self.http_post('/api/v1/order/buy', self.get_request_data(params))
        else:
            result = self.http_post('/api/v1/order/sell', self.get_request_data(params))
        return result['data']['result']['orderNo']

    def cancel_order(self, base_currency, quote_currency, order_id):
        """
			There was one time that I got 'failure' in 'operate' but the
			order was actually cancelled.
		"""
        params = {'orderNo': order_id}
        result = self.http_post('/api/v1/order/cancel', self.get_request_data(params))
        if result['data']['result']['operate'] == 'success':
            return True
        else:
            return False

    def get_order_details(self, base_currency, quote_currency, order_id):
        params = {'orderNoList': str(order_id)}
        result = self.http_post('/api/v1/order/details', self.get_request_data(params))
        return {
            'timestamp': result['data']['result']['details']['utcDeal'],
            'order_id': str(result['data']['result']['orderNo']),
            'order_type': result['data']['result']['action'].lower(),
            'price': float(result['data']['result']['priceLimit']),
            'filled_quantity_in_quote': float(result['data']['result']['details']['volume']),
            'filled_quantity_in_base': 
			float(result['data']['result']['details']['volume'])*
			float(result['data']['result']['priceLimit']),
            'fee_in_base': float(result['data']['result']['fee'])
		}
