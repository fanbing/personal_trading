import sys
import traceback
import base64
import datetime
import hashlib
import hmac
import requests
import json
import urllib
import urllib.parse
import urllib.request
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.serialization import load_pem_private_key
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives.asymmetric.utils import decode_dss_signature


class HadaxAPI():
    def __init__(self, api_key, api_secret):
        # try:
        #     '''
        #         Please generate your `privatekey.pem` file locally and add the corresponding `publickey.pem` in the Huobi exchange
        #         The `privatekey.pem` file should be located under the root directory (same as config.json)
        #         To generate these files, use the commands below
        #             openssl ecparam -name secp256k1 -genkey -noout -out privatekey.pem
        #             openssl ec -in privatekey.pem -pubout -out publickey.pem
        #     '''
        #     self.SELF_PRI = open('privatekey.pem', 'rb').read()
        # except Exception as err:
        #     print('CONFIG ERROR: ' + str(err))
        #     print(traceback.format_exc())
        #     sys.exit()

        self.API_PUB = api_key
        self.API_PRI = api_secret
        self.MARKET_URL = 'https://api.hadax.com/market'
        self.TRADE_URL = 'https://api.hadax.com/v1'
        self.ACCOUNT_ID = self.get_accounts()['data'][0]['id']


    def http_get(self, url, params, add_to_headers=None):
        '''
            REST GET wrapper
        '''
        headers = {
            "Content-type": "application/x-www-form-urlencoded",
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36',
        }
        if add_to_headers:
            headers.update(add_to_headers)

        postdata = urllib.parse.urlencode(params)
        response = requests.get(url, postdata, headers=headers, timeout=5)
        try:
            if response.status_code == 200:
                return response.json()
            else:
                return
        except BaseException as e:
            print("httpGet failed, detail is:%s,%s" % (response.text, e))
            return


    def http_post(self, url, params, add_to_headers=None):
        '''
            REST POST wrapper
        '''
        headers = {
            "Accept": "application/json",
            'Content-Type': 'application/json'
        }
        if add_to_headers:
            headers.update(add_to_headers)
        postdata = json.dumps(params)
        response = requests.post(url, postdata, headers=headers, timeout=10)
        try:
            if response.status_code == 200:
                return response.json()
            else:
                return
        except BaseException as e:
            print("httpPost failed, detail is:%s,%s" % (response.text, e))
            return


    def api_key_get(self, params, request_path):
        '''
            Function used when api key is required (on trading purpose only)
            Wrapping the double signatures into url for REST quering
        '''
        method = 'GET'
        timestamp = datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S')
        params.update({
            'AccessKeyId': self.API_PUB,
            'SignatureMethod': 'HmacSHA256',
            'SignatureVersion': '2',
            'Timestamp': timestamp
        })

        host_url = self.TRADE_URL
        host_name = urllib.parse.urlparse(host_url).hostname
        host_name = host_name.lower()
        params['Signature'] = self.build_sign(params, method, host_name, '/v1'+request_path, self.API_PRI)
        # un-comment the next line if the new signature is applied
        # params['PrivateSignature'] = self.build_pri_sign(params['Signature'])
        url = host_url + request_path
        return self.http_get(url, params)


    def api_key_post(self, params, request_path):
        '''
            Function used when api key is required (on trading purpose only)
            Wrapping the double signatures into url for REST quering
        '''
        method = 'POST'
        timestamp = datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S')
        params_to_sign = {
            'AccessKeyId': self.API_PUB,
            'SignatureMethod': 'HmacSHA256',
            'SignatureVersion': '2',
            'Timestamp': timestamp
        }
        host_url = self.TRADE_URL
        host_name = urllib.parse.urlparse(host_url).hostname
        host_name = host_name.lower()
        params_to_sign['Signature'] = self.build_sign(params_to_sign, method, host_name, '/v1' + request_path, self.API_PRI)
        # un-comment the next line if the new signature is applied
        # params_to_sign['PrivateSignature'] = self.build_pri_sign(params_to_sign['Signature'])
        url = host_url + request_path + '?' + urllib.parse.urlencode(params_to_sign)
        return self.http_post(url, params)


    def build_sign(self, passed_params, method, host_url, request_path, secret_key):
        sorted_params = sorted(passed_params.items(), key=lambda d: d[0], reverse=False)
        encode_params = urllib.parse.urlencode(sorted_params)
        payload = [method, host_url, request_path, encode_params]
        payload = '\n'.join(payload)
        payload = payload.encode(encoding='UTF8')
        secret_key = secret_key.encode(encoding='UTF8')
        digest = hmac.new(secret_key, payload, digestmod=hashlib.sha256).digest()
        signature = base64.b64encode(digest)
        signature = signature.decode()
        return signature

    # un-comment the next two functions if the new signature is applied
    # def build_pri_sign(self, signature):
    #     data = bytes(signature, encoding='utf8')
    #     # Read the pri_key_file
    #     digest = hashes.Hash( hashes.SHA256(), default_backend())
    #     digest.update(data)
    #     skey = load_pem_private_key( self.SELF_PRI, password=None, backend=default_backend())
    #     sig_data = skey.sign( data, ec.ECDSA(hashes.SHA256()))
    #     sig_r, sig_s = decode_dss_signature(sig_data)
    #     sig_bytes = b''
    #     key_size_in_bytes = self.bit_to_bytes(skey.public_key().key_size)
    #     sig_r_bytes = sig_r.to_bytes(key_size_in_bytes, "big")
    #     sig_bytes += sig_r_bytes
    #     sig_s_bytes = sig_s.to_bytes(key_size_in_bytes, "big")
    #     sig_bytes += sig_s_bytes
    #     return base64.b64encode(sig_bytes)


    # def bit_to_bytes(self, a):
    #     return (a + 7) // 8


    def get_accounts(self):
        path = "/account/accounts"
        params = {}
        return self.api_key_get(params, path)


    # Get the entire balances of the account
    def get_balances(self):
        url = "/hadax/account/accounts/{0}/balance".format(self.ACCOUNT_ID)
        params = {"account-id": self.ACCOUNT_ID}
        return self.api_key_get(params, url)


    def get_precision(self, base_currency, quote_currency):   
        quote_currency = quote_currency.lower()
        base_currency = base_currency.lower()
        if base_currency == 'usdt':
            endpoint = '/common/symbols'
            r = requests.get(self.TRADE_URL + endpoint)
        elif quote_currency == 'eth' and base_currency == 'btc':
            endpoint = '/common/symbols'
            r = requests.get(self.TRADE_URL + endpoint)
        else:
            endpoint = '/hadax/common/symbols'
            r = requests.get(self.TRADE_URL + endpoint)
        combos = r.json()['data']
        for combo in combos:
            if combo['base-currency'] == quote_currency and combo['quote-currency'] == base_currency:
                return {
                    'price_precision': int(combo['price-precision']),
                    'quantity_precision_in_quote': int(combo['amount-precision'])
                }   


    def get_orderbook(self, base_currency, quote_currency, limit):
        """

        """
        symbol = quote_currency.lower() + '' + base_currency.lower()
        endpoint = '/depth?symbol=' + symbol + '&type=step0'
        r = requests.get(self.MARKET_URL + endpoint)   
        data = r.json()['tick']
        dict = {'asks': [], 'bids': []}
        try:
            for ask in data['asks']:
                price = float(ask[0])
                size_in_quote = float(ask[1])
                dict['asks'].append({
                    'price': float(price),
                    'size_in_quote': float(size_in_quote),
                    'size_in_base': float(round(price * size_in_quote, 6))
                })
            for bid in data['bids']:
                price = float(bid[0])
                size_in_quote = float(bid[1])
                dict['bids'].append({
                    'price': float(price),
                    'size_in_quote': float(size_in_quote),
                    'size_in_base': float(round(price * size_in_quote, 6))
                })
            return dict
        except Exception as err:
            print("ORDER BOOK ERROR: " + str(err))
            print(traceback.format_exc())
            
        

    def get_kline(self, base_currency, quote_currency, granulation, limit):
        symbol = quote_currency.lower() + '' + base_currency.lower()
        endpoint = '/history/kline?period=' + granulation + '&size=' + str(limit) + '&symbol=' + symbol
        r = requests.get(self.MARKET_URL + endpoint)
        data = r.json()['data']
        list = []
        try:
            for chunk in data:
                list.append({
                    'open': float(chunk['open']),
                    'close': float(chunk['close']),
                    'low': float(chunk['low']),
                    'high': float(chunk['high']),
                    'volume_in_quote': float(0),
                    'volume_in_base': float(chunk['vol'])
                })    
            return list
        except Exception as err:
            print("KLINE ERROR: " + str(err))
            print(traceback.format_exc())     


    def get_market_trade_history(self, base_currency, quote_currency, limit):
        base_currency = base_currency.lower()
        quote_currency = quote_currency.lower()
        symbol = quote_currency + '' + base_currency
        endpoint = '/history/trade?symbol=' + symbol + '&size=' + str(limit)
        r = requests.get(self.MARKET_URL + endpoint) 
        result = r.json()['data']
        list = []
        try:
            for trade in result:
                list.append({
                    'price': float(trade['data'][0]['price']),
                    'timestamp': int(trade['ts']),
                    'size_in_quote': float(trade['data'][0]['amount']),
                    'order_type': str(trade['data'][0]['direction'])
                })
            return list
        except Exception as err:
            print('MARKET HISTORY ERROR: ' + str(err))
            print(traceback.format_exc())


    def get_balance(self, coin_name):
        coin_name = str(coin_name).lower()
        balances = self.get_balances()['data']['list']
        data = []
        try:
            for balance in balances:
                if balance['currency'] == coin_name:
                    data.append(balance)
            if data:
                return {
                    'available_balance': float(data[0]['balance']), 
                    'reserved_balance': float(data[1]['balance'])
                }
        except Exception as err:
            print('BALANCE ERROR: ' + str(err))
            print(traceback.format_exc())


    def get_open_order(self, base_currency, quote_currency):
        symbol = quote_currency.lower() + '' + base_currency.lower()
        url = '/order/openOrders'
        params = {'account-id': self.ACCOUNT_ID, 'symbol': symbol}
        orders_info = []
        result = self.api_key_get(params, url)
        if result:
            orders = result['data']
            for order in orders:
                if order['type'] == 'buy-limit':
                    order_type = 'buy'
                elif order['type'] == 'sell-limit':
                    order_type = 'sell'
                else:
                    order_type = 'unknown'
                orders_info.append({
                    'order_id': str(order['id']),
                    'order_type': str(order_type),
                    'price': float(order['price']),
                    'filled_quote': float(order['filled-amount'])
                })
            return orders_info


    def place_order(self, base_currency, quote_currency, order_type, price, size_in_quote_currency):
        symbol = quote_currency.lower() + '' + base_currency.lower()
        precision = self.get_precision(base_currency, quote_currency)
        price = round(float(price), precision['price_precision'])
        if str(precision['quantity_precision_in_quote']) == '0':
            size_in_quote_currency = int(size_in_quote_currency)
        else:
            size_in_quote_currency = round(float(size_in_quote_currency), precision['quantity_precision_in_quote'])
        if order_type == 'buy':
            exchange_order_type = 'buy-limit'
        elif order_type == 'sell':
            exchange_order_type = 'sell-limit'
        else:
            exit('PLACE ORDER ERROR')

        params = {
            "account-id": self.ACCOUNT_ID,
            "amount": size_in_quote_currency,
            "symbol": symbol,
            "type": exchange_order_type,
            "price": price,
            "source": ''
        }

        url = '/hadax/order/orders/place'
        order_id = self.api_key_post(params, url)['data']
        return str(order_id)

    
    def cancel_order(self, base_currency, quote_currency, order_id, order_type):
        params = {}
        url = "/order/orders/{0}/submitcancel".format(order_id)
        print('url')
        r = self.api_key_post(params, url)
        if r['status'] == 'ok':
            return True
        else:
            return False

    
    def get_order_details(self, base_currency, quote_currency, order_id, order_type):
        """
            Huobi charges fee in terms of the currency you will get. Say you place an order to buy
            100 CBC, the result will be you used the same amount to buy 99.8 CBC.  This means if 
            you want to buy 100 CBC, you have to place an order to buy 100/0.998 CBC.
            When you are selling 100 CBC into BTC, the result is that you will only get 0.998*price*100
            amount of BTC. The returned data 'field-fee' has already accounted this change.
        """
        url = '/order/orders/{0}'.format(order_id)
        params = {}
        result = self.api_key_get(params, url)
        if result:
            order_details = {}
            r = result['data']
            order_details = {
                'status' : str(r['state']),
                'timestamp': int(r['created-at']),
                'order_id': str(order_id),
                'order_type': str(order_type),
                'price': float(r['price']),
                'filled_quantity_in_quote': float(r['field-amount']),
                'filled_quantity_in_base': float(r['field-cash-amount']),
                'fee_in_base': float(r['field-fees']),
            }
            if r['type'] == 'buy-limit':
                order_type = 'buy'
                order_details['fee_unit'] = quote_currency
            elif r['type'] == 'sell-limit':
                order_type = 'sell'
                order_details['fee_unit'] = base_currency
            return order_details
