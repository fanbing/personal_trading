import traceback
import os
import json
import http.client
import urllib
import hashlib
import hmac
import requests
import time
import configparser
from prettyprinter import cpprint


class GateioAPI():
    def __init__(self, api_key, api_secret):
        self.API_PUB = api_key
        self.API_PRI = api_secret
        self.MARKET_URL = 'https://data.gate.io/api2/1'
        self.PRIVATE_URL = 'api.gate.io'
        self.ACCOUNT_ID = 'toBeDetermined'
            

    def get_sign(self, params):
        """
        Create signature for post function

        Parameters
        ----------
        params : dict

        Returns
        -------
        string
            Hash of all the parameters according to standard
        
        """
        b_api_pri = bytes(self.API_PRI, encoding='utf8')
        sign = ''
        for key in params.keys():
            value = str(params[key])
            sign += key + '=' + value + '&'
        b_sign = bytes(sign[:-1], encoding='utf8')

        signature = hmac.new(b_api_pri, b_sign, hashlib.sha512).hexdigest()
        return signature


    def http_post(self, host, endpoint, params):
        """
        Post function for http request.
        Since Gate.io does not support request.post() function

        Parameters
        ----------
        host: string 
            access point for public or private channel
        endpoint: string
            service required
        params: dict
            parameters attached to the query link

        Returns
        -------
        string
            Hash of all the parameters according to standard
        """
        headers = {
            "Content-type": "application/x-www-form-urlencoded",
            "KEY": self.API_PUB,
            "SIGN": self.get_sign(params)
        }

        conn = http.client.HTTPSConnection(host, timeout=10)

        temp_params = urllib.parse.urlencode(params) if params else ''

        conn.request("POST", endpoint, temp_params, headers)
        response = conn.getresponse()
        data = response.read().decode('utf-8')
        params.clear()
        conn.close()
        return data


    def get_precision(self, base_currency, quote_currency):
        symbol = quote_currency.lower() + '_' + base_currency.lower()
        endpoint = '/marketinfo'
        r = requests.get(self.MARKET_URL + endpoint)
        pairs = r.json()['pairs']
        for pair in pairs:
            for key, value in pair.items():
                if key == symbol:
                    return {
                        'price_precision': int(value['decimal_places']),
                        # 'quantity_precision_in_quote': int(len(str(value['min_amount']).split('.')[1]))
                        # Except ETH/BTC ETH/USDT BTC/USDT, most pairs are using 3 as amount_precision
                        # However, this info is not specified in the API. So, we use static integer as output
                        'quantity_precision_in_quote': int(3)
                    }
            

    def get_orderbook(self, base_currency, quote_currency, limit):
        """
            The limit must be smaller than 20.
        """
        symbol = quote_currency.lower() + '_' + base_currency.lower()
        if limit > 20:
            limit = input("""The orderbook from Gateio is only 
                            availble for the depth of 20, 
                            please enter a smaller limit: """)
        endpoint = '/orderBook/' + symbol
        r = requests.get(self.MARKET_URL + endpoint)   
        asks = r.json()['asks']
        asks.reverse()  # Gate.io API returns lowest ask as the last element in 'asks'
        bids = r.json()['bids']
        dict = {'asks': [], 'bids': []}
        try:
            for ask in asks[:limit]:
                price = float(ask[0])
                size_in_quote = float(ask[1])
                dict['asks'].append({
                    'price': float(price),
                    'size_in_quote': float(size_in_quote),
                    'size_in_base': float(round(price * size_in_quote, 3))
                })
            for bid in bids[:limit]:
                price = float(bid[0])
                size_in_quote = float(bid[1])
                dict['bids'].append({
                    'price': float(price),
                    'size_in_quote': float(size_in_quote),
                    'size_in_base': float(round(price * size_in_quote, 3))
                })
            return dict
        except Exception as err:
            print("ORDER BOOK ERROR: " + str(err))
            print(traceback.format_exc())            
        

    def get_kline(self, base_currency, quote_currency, granulation, limit):
        symbol = quote_currency.lower() + '_' + base_currency.lower()
        r = requests.get(self.MARKET_URL + '/ticker/' + symbol)
        data = r.json()
        list = []
        try:
            list.append({
                'open' : float(0),
                'close': float(data['last']),
                'low':  float(data['low24hr']),
                'high': float(data['high24hr']),
                'volume_in_quote': float(data['quoteVolume']),
                'volume_in_base':  float(data['baseVolume'])
            })   
            return list
        except Exception as err:
            print("KLINE ERROR: " + str(err))
            print(traceback.format_exc())     


    def get_market_trade_history(self, base_currency, quote_currency, limit):
        symbol = quote_currency.lower() + '_' + base_currency.lower()
        endpoint = '/tradeHistory/' + symbol
        r = requests.get(self.MARKET_URL + endpoint) 
        result = r.json()['data']
        list = []
        try:
            for trade in result:
                list.append({
                    'price': float(trade['rate']),
                    'timestamp': int(trade['timestamp']),
                    'size_in_quote': float(trade['amount']),
                    'order_type': str(trade['type'])
                })
            return list
        except Exception as err:
            print('MARKET HISTORY ERROR: ' + str(err))
            print(traceback.format_exc())


    def get_balance(self, coin_name):
        url = '/api2/1/private/balances'
        params = {}
        result = json.loads(self.http_post(self.PRIVATE_URL, url, params))
        try: 
            return {
                'available_balance': float(result['available'][coin_name.upper()]), 
                'reserved_balance': float(result['locked'][coin_name.upper()])
            }
        except Exception as err:
            print('BALANCE ERROR: ' + str(err))
            print(traceback.format_exc())
        

    def get_open_order(self, base_currency, quote_currency):
        symbol = quote_currency.upper() + '_' + base_currency.upper()
        url = '/api2/1/private/openOrders'
        params = {}
        result = json.loads(self.http_post(self.PRIVATE_URL, url, params))
        
        orders = result['orders']
        orders_info = []
        if orders:
            for order in orders:
                params = {'currencyPair': symbol, 'orderNumber': order['orderNumber']}
                url = '/api2/1/private/getOrder'
                order_status = json.loads(self.http_post(self.PRIVATE_URL, url, params))
                orders_info.append({
                    'order_id': str(order['orderNumber']),
                    'order_type': str(order['type']),
                    'price': float(order['rate']),
                    'filled_quote': float(order_status['order']['amount']) 
                })
            return orders_info
        


    def place_order(self, base_currency, quote_currency, order_type, price, size_in_quote_currency):
        try:
            symbol = quote_currency.upper() + '_' + base_currency.upper()
            precision = self.get_precision(base_currency, quote_currency)
            price = round(float(price), precision['price_precision'])
            if str(precision['quantity_precision_in_quote']) == '0':
                size_in_quote_currency = int(size_in_quote_currency)
            else:
                size_in_quote_currency = round(float(size_in_quote_currency), precision['quantity_precision_in_quote'])
            
            url = '/api2/1/private/' + order_type
            params = {'currencyPair': symbol, 'rate': price, 'amount': size_in_quote_currency}
            result = self.http_post(self.PRIVATE_URL, url, params)
            return str(json.loads(result)['orderNumber'])
        except Exception as err:
            print('PLACE ORDER ERROR: ' + str(err))
            print(traceback.format_exc())

    
    def cancel_order(self, base_currency, quote_currency, order_id, order_type):
        symbol = quote_currency.upper() + '_' + base_currency.upper()
        url = '/api2/1/private/cancelOrder'
        params = {'orderNumber': str(order_id), 'currencyPair': symbol}
        result = json.loads(self.http_post(self.PRIVATE_URL, url, params))
        cpprint(result)
        if result['result']:
            return True
        else:
            return False


    def get_order_details(self, base_currency, quote_currency, order_id, order_type):
        """
            For gate.io, the fee unit and fee amount are actually given explicitly in
            the return value of the api call.
        """
        symbol = quote_currency.upper() + '_' + base_currency.upper()
        params = {'currencyPair': symbol, 'orderNumber': order_id}
        url = '/api2/1/private/getOrder'
        order_status = json.loads(self.http_post(self.PRIVATE_URL, url, params))['order']
        if order_status:
            price = float(order_status['filledRate'])
            filled_quantity_in_quote = float(order_status['filledAmount'])
            filled_quantity_in_base = round(price * filled_quantity_in_quote, 8)
            if order_status['status'] == 'open':
                if filled_quantity_in_quote == 0:
                    status = 'unfilled'
                else:
                    status = 'partial'
                status = 'unfilled'
            else:
                status = 'filled'
            return {
                'timestamp': int(order_status['timestamp']),
                'order_id': str(order_id),
                'order_type': str(order_status['type']),
                'price': price,
                'filled_quantity_in_quote': filled_quantity_in_quote,
                'filled_quantity_in_base': filled_quantity_in_base,
                'fee': float(order_status['fee'].split(' ')[0]),
                'fee_unit': order_status['feeCurrency'],
                'status': status
            }
