"""
Binance Exchange File
"""
import requests
import json
import time
import hmac
import hashlib

RATE_LIMIT = 429
IP_BAN = 418


class BinanceAPI():
    def __init__(self, api_key, api_secret):
        self.API_PUB = api_key
        self.API_PRI = api_secret
        self.API_URL = 'https://api.binance.com'
        self.ACCOUNT_ID = api_key

    def _generate_signature(self, query_str):
        key = self.API_PRI.encode('utf-8')
        qs = query_str.encode('utf-8')
        msg = hmac.new(key, qs, hashlib.sha256)
        return msg.hexdigest()

    def get_precision(self, base_currency, quote_currency):
        symbol = base_currency.upper() + quote_currency.upper()
        endpoint = '/api/v1/exchangeInfo'
        url = self.API_URL + endpoint

        result = requests.get(url)
        data = json.loads(result.text)

        symbols_data = data['symbols']
        precision_data = [d for d in symbols_data if d['symbol'] == symbol][0]

        return {'price_precision': precision_data['baseAssetPrecision'],
                'quantity_precision_for_quote': precision_data['quotePrecision']}

    def get_orderbook(self, base_currency, quote_currency, limit=None):
        """
          If no limit is provided, then default is 100.
          Max 1000. Valid limits:[5, 10, 20, 50, 100, 500, 1000]
        """
        # ToDo: handle exchange call failure
        # ToDo: validate limit
        endpoint = '/api/v1/depth'
        symbol = base_currency.upper() + quote_currency.upper()
        limit = limit
        url = self.API_URL + endpoint + '?symbol=' + symbol
        if limit:
            url += '&limit=' + str(limit)

        headers = {'X-MBX-APIKEY': self.API_PUB}
        # ToDo: Handle Rate Limit and IP Ban
        result = requests.get(url, headers=headers)
        data = json.loads(result.text)
        orderbook = {'asks':[], 'bids':[]}
        for i in range(limit):
            orderbook['asks'].append({
                'price': float(data['asks'][i][0]),
                'size_in_quote': float(data['asks'][i][1]),
                'size_in_base': float(data['asks'][i][0])*float(data['asks'][i][1])
            })
            orderbook['bids'].append({
                'price': float(data['bids'][i][0]),
                'size_in_quote': float(data['bids'][i][1]),
                'size_in_base': float(data['bids'][i][0])*float(data['bids'][i][1])
            })
        return orderbook

    def get_kline(self, base_currency, quote_currency, granulation, limit=None):
        """
        If no limit provided, then default is 500
        granulation is used as interval for Binance.
        Supported Kline/Candlestick chart intervals:
        m -> minutes; h -> hours; d -> days; w -> weeks; M -> months
        1m
        3m
        5m
        15m
        30m
        1h
        2h
        4h
        6h
        8h
        12h
        1d
        3d
        1w
        1M
        """
        symbol = base_currency.upper() + quote_currency.upper()
        endpoint = '/api/v1/klines'
        url = self.API_URL + endpoint + "?symbol=" + symbol + "&interval=" + granulation
        if limit:
            url += "&limit=" + str(limit)
        else:
            limit = 500

        result = requests.get(url)
        data = json.loads(result.text)
        kline_info = []
        for i in range(limit):
            o = float(data[i][1])
            c = float(data[i][4])
            l = float(data[i][3])
            h = float(data[i][2])
            vq = float(data[i][7])
            vb = float(data[i][5])
            kline_info.append({'open': o, 'close': c, 'low': l, 'high': h,
                               'volume_in_quote': vq, 'volume_in_base': vb})
        return kline_info

    def get_market_trade_history(self, base_currency, quote_currency, limit=None):
        """
          If no limit is provided, then default is 500. Max 1000.
          order_type: if a buyer is a maker, it is a sell, else a buy
        """
        # ToDo valitade limit
        symbol = base_currency.upper() + quote_currency.upper()
        endpoint = '/api/v1/trades'
        url = self.API_URL + endpoint + "?symbol=" + symbol
        if limit:
            url += "&limit=" + str(limit)
        else:
            limit = 500

        result = requests.get(url)
        data = json.loads(result.text)
        trade_history = []

        for trade in data:
            p = float(trade['price'])
            sq = float(trade['qty'])
            t = trade['time']
            ot = 'SELL' if trade['isBuyerMaker'] else 'BUY'
            trade_history.append({'price': p, 'size_in_quote': sq,
                                  'timestamp': t, 'order_type': ot})
        return trade_history

    def get_balance(self, coin_name):
        endpoint = '/api/v3/account'
        headers = {'X-MBX-APIKEY': self.API_PUB}
        # Convert time to milliseconds
        current_time = str(int(time.time()) * 1000)
        query_str = "timestamp=" + current_time
        signature = self._generate_signature(query_str)

        url = self.API_URL + endpoint + "?" + query_str + "&signature=" + signature
        result = requests.get(url, headers=headers)
        data = json.loads(result.text)

        balance_data = [d for d in data['balances'] if d['asset'] == coin_name][0]
        available_balance = float(balance_data['free'])
        reserved_balance = float(balance_data['locked'])
        return {'available_balance': available_balance,
                'reserved_balance': reserved_balance}

    def get_open_order(self, base_currency, quote_currency):
        symbol = base_currency.upper() + quote_currency.upper()
        endpoint = '/api/v3/openOrders'
        headers = {'X-MBX-APIKEY': self.API_PUB}

        current_time = str(int(time.time()) * 1000)
        query_str = "timestamp=" + current_time + "&symbol=" + symbol
        signature = self._generate_signature(query_str)

        url = self.API_URL + endpoint + "?" + query_str + "&signature=" + signature
        result = requests.get(url, headers=headers)

        open_orders = []
        data = json.loads(result.text)
        for order in data:
            order_id = order['orderId']
            order_type = order['side']
            price = float(order['price'])
            fq = float(order['executedQty'])
            open_orders.append({'order_id': order_id, 'order_type': order_type,
                                'price': price, 'filled_quote': fq})

        return open_orders

    def place_order(self, base_currency, quote_currency, order_type,
                    price, size_in_quote_currency):
        """
          Binance accepts Price in DECIMAL(float) format.
          timeInForce, quantity, price are mandotory whith type=LIMIT
          GTC = Good Till Cancelled
        """
        endpoint = '/api/v3/order'
        headers = {'X-MBX-APIKEY': self.API_PUB}
        symbol = base_currency.upper() + quote_currency.upper()
        current_time = str(int(time.time()) * 1000)
        
        query_str = "timeInForce=GTC" + "&symbol=" + symbol + "&side=" + order_type + "&type=LIMIT" + "&quantity=" + str(size_in_quote_currency) + "&timestamp=" + current_time + "&price=" + str(price)
        signature = self._generate_signature(query_str)

        url = self.API_URL + endpoint + "?" + query_str + "&signature=" + signature
        result = requests.post(url, headers=headers)
        data = json.loads(result.text)
        return data['orderId']

    def cancel_order(self, base_currency, quote_currency, order_id):
        endpoint = '/api/v3/order'
        symbol = base_currency.upper() + quote_currency.upper()
        current_time = str(int(time.time()) * 1000)
        headers = {'X-MBX-APIKEY': self.API_PUB}
        query_str = "symbol=" + symbol + "&orderId=" + order_id + "&timestamp=" + current_time
        signature = self._generate_signature(query_str)

        url = self.API_URL + endpoint + "?" + query_str + "&signature=" + signature
        result = requests.delete(url, headers=headers)
        data = json.loads(result.text)

        if data['clientOrderId']:
            return True
        else:
            return False

    def get_order_details(self, base_currency, quote_currency, order_id, order_type):
        endpoint = '/api/v3/order'
        symbol = base_currency.upper() + quote_currency.upper()
        current_time = str(int(time.time()) * 1000)
        headers = {'X-MBX-APIKEY': self.API_PUB}
        query_str = "symbol=" + symbol + "&timestamp=" + current_time + "&orderId=" + order_id
        signature = self._generate_signature(query_str)

        url = self.API_URL + endpoint + "?" + query_str + "&signature=" + signature
        result = requests.get(url, headers=headers)
        data = json.loads(result.text)

        return {
            'timestamp': data['time'],
            'order_id': str(data['orderId']),
            'order_type': data['side'],
            'price': float(data['price']),
            'filled_quantity_in_quote': float(data['executedQty']),
            'fee': 0.0,
            'fee_unit': None,
            'status': data['status']
        }
