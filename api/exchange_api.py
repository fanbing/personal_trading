from api.exchanges.coinsuper import CoinsuperAPI
from api.exchanges.gateio import GateioAPI
from api.exchanges.huobi import HuobiAPI
from api.exchanges.hadax import HadaxAPI
from api.exchanges.kucoin import KucoinAPI
from api.exchanges.bittrex import BittrexAPI
from api.exchanges.binance import BinanceAPI
import traceback
from prettyprinter import cpprint
import time
# from api.exchanges.gateio import GateioAPI
# import utils.logging as log


class ExchangeAPI:
    """
    Initalizes and returns an api object providing an interface to
    the exchange provided.

    Currently supporting:

        # Bitfinex
        Coinsuper
        Gateio
        Hadax
        Huobi
        Kucoin
        Bittrex
        Binance

    Standard functions supported:

    >>> get_precision()

    >>> get_orderbook()

    >>> get_kline()

    >>> get_market_trade_history()

    >>> get_balance()

    >>> get_open_order()

    >>> place_order()

    >>> cancel_order()

    >>> get_order_details()

    Parameters
    ----------
    exchange_name : string
        Name of the exchange in caps (ie.  "BITTREX", "COINSUPER", "GATEIO",
                                           "HADAX", "HUOBI", "KUCOIN", "BINANCE")
    credentials : dict
        {
            "API_KEY": 'xxx'
            "API_SECRET": 'xxx'
        }
        Api key/secret combo needed for the exchange.

    Returns
    -------
    dict
        An object with callable functions.

    See Also
    --------
    Bitfinex:
    Coinsuper:
    Gateio:
    Hadax & Huobi: https://github.com/huobiapi/API_Docs_en
    Kucoin: https://kucoinapidocs.docs.apiary.io/
    Bittrex: https://github.com/ericsomdahl/python-bittrex/blob/master/bittrex/bittrex.py
    

    Examples
    --------
    These are written in doctest format, and should illustrate how to
    use the function.

    >>> api = ExchangeAPI(self.exchange_name, settings["CREDENTIALS"])

    >>> balance = api.get_balance("BTC")
        0.12
    """

    def __init__(self, session_id, algo_info, credentials, db=None):
        # self.client_name = input('Please provide the name of the client: ')
        # self.algo_name = input('Please provide the name of the algo: ')
        exchange_name = algo_info['exchange_name']
        self.session_id = session_id
        cpprint('***** Please record the trade session id: %s ******' %
                str(self.session_id))
        self.algo_name = algo_info['algo_name'].upper()
        self.db = db
        if db:
            # save the same db object to use on all API call logging
            self.db_history_logger = db['bot_event_executed']
            self.db_trade_logger = db['bot_event_placed']
        if exchange_name == "KUCOIN":
            self.exchange_name = "KUCOIN"
            self.client = KucoinAPI(credentials["API_KEY"],
                                    credentials["API_SECRET"])
        elif exchange_name == 'COINSUPER':
            self.exchange_name = "COINSUPER"
            self.client = CoinsuperAPI(credentials["API_KEY"],
                                       credentials["API_SECRET"])
        elif exchange_name == 'HADAX':
            self.exchange_name = "HADAX"
            self.client = HadaxAPI(credentials["API_KEY"],
                                   credentials["API_SECRET"])
        elif exchange_name == 'HUOBI':
            self.exchange_name = "HUOBI"
            self.client = HuobiAPI(credentials["API_KEY"],
                                   credentials["API_SECRET"])
        elif exchange_name == 'GATEIO':
            self.exchange_name = "GATEIO"
            self.client = GateioAPI(credentials["API_KEY"],
                                    credentials["API_SECRET"])
        elif exchange_name == 'BITTREX':
            self.exchange_name = 'BITTREX'
            self.client = BittrexAPI(credentials['API_KEY'],
                                     credentials['API_SECRET'])
        elif exchange_name == 'BINANCE':
            self.exchange_name = 'BINANCE'
            self.client = BinanceAPI(credentials['API_KEY'],
                                     credentials['API_SECRET'])
        else:
            print('Exchange not compatible yet.')

    def get_precision(self, base_currency, quote_currency):
        """
        Gets the allowed currency precision for specific trading pair.

        Parameters
        ----------
        base_currency : string, 'btc'
        quote_currency : string  'cbc'
            Symbol of the currency you want in lower cases (ie. "btc")

        Returns
        -------
        dict
            {'price_precision': int 0~9,
             'quantity_precision_in_quote': int 0~9}

            If precision is 0, the required arguments in placing orders need to be
            restricted to integer.
        """
        precision = self.client.get_precision(base_currency, quote_currency)
        return precision

    def get_orderbook(self, base_currency, quote_currency, limit):
        """
        Gets the order information posted in the market.

        Parameters
        ----------
        base_currency : string
        quote_currency : string
        limit : int 10

            Symbol of the currency you want in lower cases (ie. "btc")

        Returns
        -------
        dict
            {
            'asks':[{'price' : float, 'size_in_quote' : float, 'size_in_base' : float},...],
            'bids':[{'price' : float, 'size_in_quote' : float, 'size_in_base' : float},...]
            }

            For arrays, the entries are ranked by price or asks, in ascending order,
            for bids, in decending order.
        """
        order_info = self.client.get_orderbook(base_currency, quote_currency,
                                               limit)
        return order_info

    def get_kline(self, base_currency, quote_currency, granulation, limit):
        """
        Gets the kline information in the past granulation period.

        Parameters
        ----------
        base_currency : string
        quote_currency : string
        granulation: string ['1min', '5min', '15min', '30min', '1hour', '8hour', '1day', '1week']
        limit : int

            Symbol of the currency you want in lower cases (ie. "btc")

        Returns
        -------
        list
            [{'open' : float, 'close' : float, 'low': float, 'high': float, 
            'volume_in_quote' : float, 'volume_in_base' : float}, ...]

            For volume_in_quote, if exchange has no support on this, return float(0)

        """
        ticker_info = self.client.get_kline(base_currency, quote_currency,
                                            granulation, limit)
        return ticker_info

    def get_market_trade_history(self, base_currency, quote_currency, limit):
        """
        Get trade history in the market of a certain trading pair.

        Parameters
        ----------
        base_currency : string
        quote_currency : string
        limit : integer

            Symbol of the currency you want in lower cases (ie. "btc")

        Returns
        -------
        list
            [{'price': float, 'size_in_quote': float, 'timestamp': int, 
            'order_type': string}, ...]

            The returned array is ranked by timestamp in descending order.
        """
        market_trade_history = self.client.get_market_trade_history(
            base_currency, quote_currency, limit)
        return market_trade_history

    def get_balance(self, coin_name):
        """
        Gets the balance of a certain coin in the account.

        Parameters
        ----------
        coin_name : string 'cbc'

            Symbol of the currency you want in lower cases (ie. "btc")

        Returns
        -------
        dict
            {'available_balance': float, 'reserved_balance': float}

            'reserved_balance' should equal to zero if there is no open order.
            We can set this as a test.
        """
        balance = self.client.get_balance(coin_name)
        return balance

    def get_open_order(self, base_currency, quote_currency, return_dict=False):
        """
        Get open orders of a specific trading pair of an account.

        Parameters
        ----------
        base_currency : string
        quote_currency : string

            Symbol of the currency you want in lower cases (ie. "btc")

        Returns
        -------
        list
            [
                {‘order_id’: string, ‘order_type’: string ['buy','sell'], ‘price’: float, 
                ‘filled_quote’: float},
                ...
            ]

            If there is no open order, return [].
        """
        open_orders = self.client.get_open_order(base_currency, quote_currency)

        # if dictonary format is requested (default is false)
        if return_dict:
            buy_orders = [order for order in open_orders if
                          order['order_type'] == 'buy']
            sell_orders = [order for order in open_orders if
                           order['order_type'] == 'sell']
            open_orders = {
                'buy': buy_orders,
                'sell': sell_orders
            }
        
        return open_orders

    def place_order(self, base_currency, quote_currency,
                    order_type, price, size_in_quote_currency):
        """
        Post an order and get the order ID.

        Parameters
        ----------
        base_currency : string
        quote_currency : string
        order_type : string  ['buy','sell']
        price : float
        size_in_quote_currency : float

            Symbol of the currency you want in lower cases (ie. "btc")

        Returns
        -------
        string
            order_id
        """
        order_id = self.client.place_order(base_currency, quote_currency,
                                           order_type.lower(), price,
                                           size_in_quote_currency)
        placed_order = {
            'exchange_name': self.exchange_name,
            'user_id': self.client.ACCOUNT_ID,
            'algo_name': self.algo_name,
            # every exchange api need to specify a account_id
            'order_id': order_id,
            'base_currency': base_currency,
            'quote_currency': quote_currency,
            'order_type': order_type,
            'price': price,
            'size_in_quote_currency': size_in_quote_currency,
            'time_stamp': int(time.time()),
            'session_id': self.session_id
        }
        cpprint('Order placed to %s %s %s at price of  %s %s' % (
            order_type, size_in_quote_currency, quote_currency,
            price, base_currency))
        if self.db:
            self.db_trade_logger.insert_one(placed_order)
        return order_id

    def cancel_order(self, base_currency, quote_currency,
                     order_id, order_type):
        """
        Cancel an open order with specific order ID.

        Parameters
        ----------
        base_currency : string
        quote_currency : string
        order_id : string
        order_type: string ['buy','sell']

            Symbol of the currency you want in lower cases (ie. "btc")

        Returns
        -------
        Boolean
            True or False
        """
        order_details = self.get_order_details(base_currency, quote_currency,
                                               order_id, order_type)
        if not order_details:
            time.sleep(5)
            order_details = self.get_order_details(base_currency,
                                                   quote_currency,
                                                   order_id, order_type)
        order_details['exchange_name'] = self.exchange_name
        order_details['user_id'] = self.client.ACCOUNT_ID
        order_details['algo_name'] = self.algo_name
        order_details['session_id'] = self.session_id
        order_details['base_currency'] = base_currency
        order_details['quote_currency'] = quote_currency
        if self.db:
            self.db_history_logger.insert_one(order_details)
        if order_details['status'] != 'filled':
            cancelled_or_not = self.client.cancel_order(
                base_currency, quote_currency, order_id, order_type)
            return cancelled_or_not
        else:
            print('Order: %s has already been cancelled' % order_id)
            return False

    def get_order_details(self, base_currency, quote_currency,
                          order_id, order_type):
        """
        Get the details of an order given a specific order ID.

        Parameters
        ----------
        base_currency : string
        quote_currency : string
        order_id : string
        order_type : string  ['buy','sell']

            Symbol of the currency you want in lower cases (ie. "btc")

        Returns
        -------
        dict
        {
             'timestamp': int, 'order_id': string, 'order_type': string, 
             'price': float, 'filled_quantity_in_quote': float, 
             'filled_quantity_in_base': float, 'fee': float, 'fee_unit': str coin_symbol
             'status': string ['filled', 'partial', 'unfilled'],
             ''}
        }
        """
        try:
            order_details = self.client.get_order_details(
                base_currency, quote_currency, order_id, order_type)
        except Exception as err:
            print('ERROR: Get order detail failed.')
            print("EXCEPTION: " + str(err))
            print(traceback.format_exc())
            return False
        return order_details
