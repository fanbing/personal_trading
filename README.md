# Trading Repo
All algos and exchanges will be placed here.

# Current Algos Supported

# Current Exchanges Supported

## Getting Started

These instructions are based on a Apple Mac OS, but should be similar to Linux/Windows; adjust accordingly.
Reccomended IDE is Visual Studio Code.

### Prerequisites

Clone the repo from the link: 
```
git clone https://gitlab.com/cm-blockchains/trading.git
```

### Installing

A step by step series of examples that tell you how to get a development env running

Download pipenv for package management

```
sudo pip install pipenv --upgrade --ignore-installed
```

Download all dependencies in the pipfile

```
pipenv install
```

Download all dependencies in the pipfile

```
pipenv install
```


### Troubleshooting for installing
If you encounter the problem of `"ValueError: unknown locale: UTF-8"`, please try these lines below
Open a new `terminal` or `cmd` window
```
nano .bash_profile
```

Add the below two lines
```
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
```

Make sure your terminal/cmd starts running with this `.bash_profile`


### Running the algorithm

Fill out the config file - NEVER UPLOAD API KEYS OR ANY CREDENTIAL INFORMATION.

```
pipenv run python main.py
```

## Running the tests

See pycco docs/comments for cases covered in testing.

```
pipenv run python -m pytest
```

### Coding Style Tests

PEP-8 Code style and formatting checks
Attempt to get atleast 5/10 for the code style score.

```
pipenv run pylint main.py
pipenv run pylint api/exchanges.py

In general: pipenv run pylint <path to file>
```

## Built With

Check Pipfile

## Pull Request Check List

Check out in Notion: https://www.notion.so/cryptomover/Submitting-PR-s-3805e489d7b74c369c969707dbadb36f

Select 'Simple' on PR via Gitlab UI.

## Contributions
#### Update the list below on major feature integrations/upgrades/changes

* **Srinjoy**:
*Templated code and initial development on all files*

