import sys
import traceback
import time
import yaml
from huobi import HuobiAPI as 


def main():
    print("hi")
    
    try:
        with open('config.yaml', 'r') as config_file:
            data = yaml.load(config_file)
            algo = Iceberg(data)
            print(algo)
            is_valid_inputs = IcebergHelpers.check_input_data(data)
            if is_valid_inputs:
                is_confirmed = IcebergHelpers.confirm_inputs(data)
    except Exception as err:
        print("STARTUP ERROR: " + str(err))
        print(traceback.format_exc())
        sys.exit()
    

    API = ExchangeAPI("KUCOIN", data["CREDENTIALS"])
    res = API.get_balance('ETH')
    print(res)