##  equires pymongo 3.6.0+
from pymongo import MongoClient
from prettyprinter import cpprint
import yaml
from utils.db import Database


with open('config.yaml', 'r') as config_file:
    # init
    data = (yaml.load(config_file))
    db_credentials = data["CREDENTIALS"]["MONGO"]
    init_db = Database(data, db_credentials)
    db = init_db.get_db()

database = db["koinbros_trading"]
collection = database["bot_event_executed"]

# Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

query = {}
query["order_type"] = u"sell"
query["exchange_name"] = u"HUOBI"
query["client"] = u"Draper"
query["base_currency"] = u"eth"
query["quote_currency"] = u"hit"

projection = {}
projection[""] = 1.0
projection["filled_quantity_in_quote"] = 1.0

sort = [ (u"price", 1) ]

cursor = collection.find(query, projection = projection, sort = sort)
total_traded = 0
try:
    for doc in cursor:
        #print(doc)
        total_traded = doc['filled_quantity_in_quote']+total_traded
finally:
    cpprint('********** %s **********' % total_traded)
    db.close()
