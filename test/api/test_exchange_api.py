import pytest
from api.exchange_api import ExchangeAPI
"""
How do you deal with different settings needed for 
different exchange clients (ie. huobi requires different credentials)
- we need to setup test accounts so we are able to use real orderids, etc.
so the tests work correctly. Or we need to use mocks to ensure the return values are correct.
"""



def get_settings(test_exchange_name):
    if test_exchange_name == "COINSUPER":
        return {
            "API_KEY": "",
            "API_SECRET": ""
        }   
    elif test_exchange_name == "GATEIO":
        return {
            "API_KEY": "",
            "API_SECRET": ""
        }
    elif test_exchange_name ==  "HADAX":
        return {
            "API_KEY": "",
            "API_SECRET": ""
        }
    elif test_exchange_name ==  "HUOBI":
        return {
            "API_KEY": "",
            "API_SECRET": ""
        }
    elif test_exchange_name ==  "KUCOIN":
        return {
            "API_KEY": "",
            "API_SECRET": ""
        }
    elif test_exchange_name == "BINANCE":
        return {
            "API_KEY": "",
            "API_SECRET": ""
        }
    print('Exchange is not available.')


@pytest.mark.parametrize("test_exchange_name", [
    ("GATEIO")
])
def test_get_precision(test_exchange_name):
    settings = get_settings(test_exchange_name)   
    algo_info = {'exchange_name': 'GATEIO', 'algo_name': 'api-testing', 'client_name': 'ready-player-2'}
    api = ExchangeAPI(algo_info, settings)
    result = api.get_precision('btc','eth')
    assert type(result) == dict
    assert type(result['price_precision']) == int
    assert type(result['quantity_precision_in_quote']) == int


@pytest.mark.parametrize("test_exchange_name", [
    ("GATEIO")
])
def test_get_orderbook(test_exchange_name):
    settings = get_settings(test_exchange_name)
    algo_info = {'exchange_name': 'GATEIO', 'algo_name': 'api-testing', 'client_name': 'ready-player-2'}
    api = ExchangeAPI(algo_info, settings)
    result = api.get_orderbook('btc','eth',10)
    assert type(result) == dict
    assert type(result['asks']) == list
    ask_len = len(result['asks'])
    assert type(result['asks'][ask_len - 1]['price']) == float
    assert type(result['asks'][ask_len - 1]['size_in_quote']) == float
    assert type(result['asks'][ask_len - 1]['size_in_base']) == float
    assert type(result['bids']) == list
    for i in range(ask_len-1):
        assert result['asks'][i]['price']<result['asks'][i+1]['price']
    bid_len = len(result['bids'])
    assert type(result['bids'][bid_len - 1]['price']) == float
    assert type(result['bids'][bid_len - 1]['size_in_quote']) == float
    assert type(result['bids'][bid_len - 1]['size_in_base']) == float
    for j in range(bid_len-1):
        assert result['bids'][j]['price']>result['bids'][j+1]['price']


@pytest.mark.parametrize("test_exchange_name", [
    ("GATEIO")
])
def test_get_kline(test_exchange_name):
    settings = get_settings(test_exchange_name)
    algo_info = {'exchange_name': 'GATEIO', 'algo_name': 'api-testing', 'client_name': 'ready-player-2'}
    api = ExchangeAPI(algo_info, settings)
    result = api.get_kline('btc', 'eth', '1day', 10)
    assert type(result) == list
    kline_count = len(result)
    assert type(result[kline_count - 1]['open']) == float
    assert type(result[kline_count - 1]['close']) == float
    assert type(result[kline_count - 1]['high']) == float
    assert type(result[kline_count - 1]['low']) == float
    assert type(result[kline_count - 1]['volume_in_quote']) == float
    assert type(result[kline_count - 1]['volume_in_base']) == float


@pytest.mark.parametrize("test_exchange_name", [
    ("GATEIO")
])
def test_get_market_trade_history(test_exchange_name):
    settings = get_settings(test_exchange_name)
    algo_info = {'exchange_name': 'GATEIO', 'algo_name': 'api-testing', 'client_name': 'ready-player-2'}
    api = ExchangeAPI(algo_info, settings)
    result = api.get_market_trade_history('btc', 'eth',  10)
    assert type(result) == list
    trade_count = len(result)
    assert type(result[trade_count - 1]['price']) == float
    assert type(result[trade_count - 1]['size_in_quote']) == float
    assert type(result[trade_count - 1]['timestamp']) == int
    assert type(result[trade_count - 1]['order_type']) == str


@pytest.mark.parametrize("test_exchange_name", [
    ("GATEIO")
])
def test_get_open_order(test_exchange_name):
    settings = get_settings(test_exchange_name)
    algo_info = {'exchange_name': 'GATEIO', 'algo_name': 'api-testing', 'client_name': 'ready-player-2'}
    api = ExchangeAPI(algo_info, settings)
    result = api.get_open_order('btc','eth')
    if result:
        assert type(result) == list
        assert type(result[0]['order_id']) == str 
        assert type(result[0]['order_type']) == str
        assert type(result[0]['price']) == float
        assert type(result[0]['filled_quote']) == float


@pytest.mark.parametrize("test_exchange_name", [
    ("GATEIO")
])
def test_get_balance(test_exchange_name):
    settings = get_settings(test_exchange_name)
    algo_info = {'exchange_name': 'GATEIO', 'algo_name': 'api-testing', 'client_name': 'ready-player-2'}
    api = ExchangeAPI(algo_info, settings)
    result = api.get_balance("rating")
    assert type(result) == dict
    assert type(result['available_balance']) == float 
    assert type(result['reserved_balance']) == float
    if api.get_open_order('btc','eth') is None:
        assert result['reserved_balance'] == 0


@pytest.mark.parametrize("test_exchange_name", [
    ("GATEIO")
])
def test_place_order(test_exchange_name):
    settings = get_settings(test_exchange_name)
    algo_info = {'exchange_name': 'GATEIO', 'algo_name': 'api-testing', 'client_name': 'ready-player-2'}
    api = ExchangeAPI(algo_info, settings)
    # To test we will put a buy order at 1/2 of the market price 
    # so the order should never be filled. But we can use the order
    # to do test.
    result = api.place_order('eth', 'loom', 'buy', 0.0003899231313123123213, 1.333333333)
    assert type(result) == str


@pytest.mark.parametrize("test_exchange_name", [
    ("GATEIO")
])
def test_cancel_order(test_exchange_name):
    settings = get_settings(test_exchange_name)
    algo_info = {'exchange_name': 'GATEIO', 'algo_name': 'api-testing', 'client_name': 'ready-player-2'}
    api = ExchangeAPI(algo_info, settings)
    result = api.cancel_order('eth', 'loom', '5b56bc556e0ba20dcd885878', 'buy')
    assert type(result) == bool


@pytest.mark.parametrize("test_exchange_name", [
    ("GATEIO")
])
def test_get_order_details(test_exchange_name):
    settings = get_settings(test_exchange_name)
    algo_info = {'exchange_name': 'GATEIO', 'algo_name': 'api-testing', 'client_name': 'ready-player-2'}
    api = ExchangeAPI(algo_info, settings)
    # To test the get order detail function, we need to
    # create an open order first.
    result = api.get_order_details('eth', 'loom', '5b56bc556e0ba20dcd885878', 'buy')
    assert type(result) == dict
    assert type(result['timestamp']) == int
    assert type(result['order_id']) == str
    assert type(result['order_type']) == str
    assert type(result['price']) == float
    assert type(result['filled_quantity_in_quote']) == float
    assert type(result['filled_quantity_in_base']) == float
    assert type(result['fee_in_base']) == float
