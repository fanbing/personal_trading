import pytest
from api.exchange_api import ExchangeAPI
import algos.twap.helpers as hps
import time
import random

def orderbook_generator(mid_price):
    orderbook_list = []
    # the senario where the first order is big enough
    orderbook1 = {'asks':[], 'bids':[]}    
    for i in range(5):
        ask_price = mid_price*(1+(i+1)/10)
        bid_price = mid_price*(1-(i+1)/10)
        size_in_quote = random.uniform(4,6)
        orderbook1['asks'].append({'price': ask_price, 'size_in_quote': size_in_quote, 'size_in_base': ask_price*size_in_quote})
        orderbook1['bids'].append({'price': bid_price, 'size_in_quote': size_in_quote, 'size_in_base': bid_price*size_in_quote})
    orderbook_list.append(orderbook1)
    # the senario where we need to fill multiple orders
    orderbook2 = {'asks':[], 'bids':[]}
    for i in range(10):
        ask_price = mid_price*(1+(i+1)/10)
        bid_price = mid_price*(1-(i+1)/10)
        size_in_quote = random.random()
        orderbook2['asks'].append({'price': ask_price, 'size_in_quote': size_in_quote, 'size_in_base': ask_price*size_in_quote})
        orderbook2['bids'].append({'price': bid_price, 'size_in_quote': size_in_quote, 'size_in_base': bid_price*size_in_quote})
    orderbook_list.append(orderbook2)
    # the senario where the orderbook is too thin for us to place order
    orderbook3 = {'asks':[], 'bids':[]}
    for i in range(5):
        ask_price = mid_price*(1+(i+1)/10)
        bid_price = mid_price*(1-(i+1)/10)
        size_in_quote = random.uniform(0,0.6)
        orderbook3['asks'].append({'price': ask_price, 'size_in_quote': size_in_quote, 'size_in_base': ask_price*size_in_quote})
        orderbook3['bids'].append({'price': bid_price, 'size_in_quote': size_in_quote, 'size_in_base': bid_price*size_in_quote})
    orderbook_list.append(orderbook3)
    return orderbook_list

orderbook_list = orderbook_generator(1)

def test_buy_market_calculator():
    rd_index = random.choice(range(3))
    result = hps.buy_market_calculator(3, orderbook_list[rd_index]['asks'])
    if rd_index==0:
        assert result[0] == 1.1
    elif rd_index==1:
        assert result[0]>= 1.3
    elif rd_index==2:
        assert result == False

def test_sell_market_calculator():
    rd_index = random.choice(range(3))
    result = hps.sell_market_calculator(3, orderbook_list[rd_index]['bids'])
    if rd_index==0:
        assert result[0] == 0.9
    elif rd_index==1:
        assert result[0]<= 0.7
    elif rd_index==2:
        assert result == False

def test_get_timestamp_list():
    duration = int(random.uniform(5000,10000))
    interval = int(random.uniform(100,200))
    result = hps.get_timestamp_list(int(time.time()), duration, interval)
    assert result[0] > time.time()
