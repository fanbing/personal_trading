import pytest
import algos.dynamic_wall.helpers as hps
import random

def test_size_calculator():
    size_list = hps.size_calculator(50)
    assert type(size_list) == list
    assert max(size_list) == size_list[4]
    assert min(size_list) == size_list[0]

def test_price_calculator():
    price_list1 = hps.price_calculator(10, 0.02, 'buy')
    price_list2 = hps.price_calculator(10, 0.02, 'sell')
    assert type(price_list1) == list
    assert len(price_list1) == 5
    assert max(price_list1) == price_list1[0]
    assert min(price_list1) == price_list1[4]
    assert type(price_list2) == list
    assert len(price_list2) == 5
    assert max(price_list2) == price_list2[4]
    assert min(price_list2) == price_list2[0]

def test_order_calculator():
    order_list = hps.order_calculator(50, 10, 'buy', 0.2, 0.02)
    assert type(order_list) == list