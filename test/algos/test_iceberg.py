import pytest
import mock
from unittest.mock import Mock
from api.exchange_api import ExchangeAPI
from algos.iceberg.main import Iceberg

def test_buy_with_limit(mocker):
    # no volume available at limit price
    settings = {
        "EXCHANGE_NAME" : "KUCOIN",
        "LIMIT_PRICE" :  1.0,
        "TOTAL_VOLUME" : 10.0,
        "MAX_ORDER_SIZE" : 5.0,
        "ALT_ORDER_SIZE" :  1.5,
        "TYPE" : "BUY",
        "TOKEN" : "ETH",
        "BASE" : "BTC",
        "SLEEP_TIME" : 10
    }
    credentials = {
        "API_KEY": "5b370f1fe0abb815f3cfa790",
        "API_SECRET": "29800557-c1db-40e6-8b3c-f62fb1ecdf72"
    }
    base = "BTC"
    token = "ETH"
    current_coin_balance = 2.0
    current_asks = [ {'price' : 1.2, 'size_in_quote' : 2, 'size_in_base' : 0.2}, {'price' : 1.4, 'size_in_quote' : 4, 'size_in_base' : 0.1}]
    current_open_orders = []
    limit_price = settings["LIMIT_PRICE"]
    max_order_size = settings["MAX_ORDER_SIZE"]
    alt_order_size = settings["ALT_ORDER_SIZE"]
    remaining_balance_needed = 7.00
    remaining_purchase_volume = 20.00

    mocker.patch.object(ExchangeAPI, 'cancel_order')
    mocker.patch.object(ExchangeAPI, 'get_precision')
    mocker.patch.object(Iceberg, 'place_buy_order')
    ExchangeAPI.cancel_order.return_value = True
    ExchangeAPI.get_precision.return_value = {'price_precision': 7, 'quantity_precision_in_quote': 8}
    algo = Iceberg(settings, credentials)
    # buy with no prev orders and no volume available
    algo.buy_with_limit(base, token, current_coin_balance, current_asks, current_open_orders, limit_price, max_order_size, alt_order_size, remaining_balance_needed, remaining_purchase_volume)
    # ensure alt order is placed
    algo.place_buy_order.assert_called_with(base, token, limit_price, alt_order_size)

def test_buy(mocker):
    # no volume available at limit price
    settings = {
        "EXCHANGE_NAME" : "KUCOIN",
        "LIMIT_PRICE" :  2.0,
        "TOTAL_VOLUME" : 10.0,
        "MAX_ORDER_SIZE" : 1.0,
        "ALT_ORDER_SIZE" :  0.5,
        "TYPE" : "BUY",
        "TOKEN" : "ETH",
        "BASE" : "BTC",
        "SLEEP_TIME" : 10
    }
    credentials = {
        "API_KEY": "5b370f1fe0abb815f3cfa790",
        "API_SECRET": "29800557-c1db-40e6-8b3c-f62fb1ecdf72"
    }
    btc_coin_balance = [1,2,3,4]
    eth_coin_balance = [1,2,3,4]
    def mock_get_balance(coin):
        if(coin == "ETH"):
            eth_coin_balance.pop()
        if(coin == "BTC"):
            btc_coin_balance.pop()
    order_book = [
        {
            'asks':[ {'price' : 1.2, 'size_in_quote' : 2, 'size_in_base' : 0.2}, {'price' : 1.4, 'size_in_quote' : 4, 'size_in_base' : 0.1}],
            'bids':[ {'price' : 1.2, 'size_in_quote' : 2, 'size_in_base' : 0.2}, {'price' : 1.4, 'size_in_quote' : 4, 'size_in_base' : 0.1}]
        },
        {
            'asks':[ {'price' : 1.2, 'size_in_quote' : 2, 'size_in_base' : 0.2}, {'price' : 1.4, 'size_in_quote' : 4, 'size_in_base' : 0.1}],
            'bids':[ {'price' : 1.2, 'size_in_quote' : 2, 'size_in_base' : 0.2}, {'price' : 1.4, 'size_in_quote' : 4, 'size_in_base' : 0.1}]
        }
    ]
    def mock_get_orderbook(base_currency, quote_currency, limit):
        # always going to be eth_btc
        return order_book.pop()

    

    mocker.patch.object(ExchangeAPI, 'get_balance')
    ExchangeAPI.get_balance.side_effect = mock_get_balance
    mocker.patch.object(ExchangeAPI, 'get_orderbook')
    ExchangeAPI.get_orderbook.side_effect = mock_get_orderbook
    mocker.patch.object(ExchangeAPI, 'get_open_order')
    ExchangeAPI.get_open_order.side_effect = [
        {'bids': [], 'asks': []},
        {'bids': [], 'asks': []}
    ]
    mocker.patch.object(ExchangeAPI, 'cancel_order')
    ExchangeAPI.cancel_order.return_value = True
    mocker.patch.object(ExchangeAPI, 'get_precision')
    ExchangeAPI.get_precision.return_value = {'price_precision': 7, 'quantity_precision_in_quote': 8}
    mocker.patch.object(Iceberg, 'place_buy_order')


    algo = Iceberg(settings, credentials)
    # buy with no prev orders and no volume available
    algo.buy()
