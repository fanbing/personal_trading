import random

def order_calculator(
		brick_in_base, 
		market_price,
		limit_price, 
		order_side, 
		distance_from_price, 
		order_gap, 
		precision,
		slice_number
	):
	"""
		This function will calculate five orders that the client want to place.
		Parameters
		----------
		brick_in_base: float
			The total amount we want to place in the unit of base currency.
		market_prie: float
			The last traded price of the pair
		order_side: string ['buy', 'sell']
		distance_from_price: float
			How much do you want to stay away from the current price, in percentage.
		order_gap: float:
			How much do you want orders to stay way from each other, in percentage.
	"""
	order_to_place = []
	order_size_list = size_calculator(brick_in_base, slice_number)
	price_list = price_calculator(market_price, limit_price, order_gap, distance_from_price, order_side, slice_number)
	for i in range(slice_number):
		order_size_in_quote = round(order_size_list[i]/price_list[i], precision)
		order_to_place.append({
			'price':price_list[i], 
			'size_in_quote':order_size_in_quote, 
			'order_type': order_side
			})
	return order_to_place

def size_calculator(brick_in_base, slice_number):
	"""
		This will calculate 5 amounts that the wall orders will be contructed with.
		The order of the sizes is from small amount to large amount.
	"""
	order_size_list = []
	min_size = 1/slice_number*1/2
	size_gap = 1/slice_number/(slice_number-1)
	for i in range(slice_number-1):
		order_size_list.append((min_size + (i-0.5+random.random())* size_gap)*brick_in_base)
	order_size_list.append(brick_in_base-sum(order_size_list))
	return order_size_list

def price_calculator(price, limit_price, gap, distance_from_price, order_side, slice_number):
	"""
		This will calculate 5 prices that the wall orders will be contructed with.
		The order of the prices is diverging away from the fair price.
	"""
	if order_side == 'buy':
		# After calculating the discouted price, the price to place is larger than limit price,
		# place starting from the limit price instead.
		if price*(1 - distance_from_price) > limit_price:
			price_list = [limit_price]
		else:
			price_list = [price*(1-distance_from_price)]
		for i in range(slice_number):
			price_list.append(price_list[i]-2*price*gap*random.random())
	elif order_side == 'sell':
		if price*(1 + distance_from_price) < limit_price:
			price_list = [limit_price]
		else:
			price_list = [price*(1+distance_from_price)]
		for i in range(slice_number):
			price_list.append(price_list[i]+2*price*gap*random.random())
	return price_list