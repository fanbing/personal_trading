import sys
import traceback
import time
import datetime
import json
from prettyprinter import cpprint
from json.decoder import JSONDecodeError
from api.exchange_api import ExchangeAPI
import algos.dynamic_wall.helpers as helpers
import utils.slack as slk

class Dynamic_wall:
    """
    Read Notion docs for specifications of this algorithm.
    The __init__ function contains further docs.
    """
    def __init__(self, settings, credentials, db, session_id):
        """
        Runs the DYNAMIC_WALL algorithm with the following settings provided in
        the config.yaml file.

        Standard functions supported:
            >> build_wall()

        Parameters
        ----------
        settings : dict
            DYNAMIC_WALL:
                EXCHANGE_NAME: KUCOIN
                CLIENT_NAME: CASHBET
                LIMIT_PRICE: 0.00001
                TOTAL_VOLUME_IN_BASE: 50
                QUOTE_CURRENCY: cbc
                BASE_CURRENCY: btc
                REFRESH_INTERVAL: 300
                DISTANCE_FROM_FAIR_PRICE: 0.2
                ORDER_GAP: 0.002
                SLICE_NUMBER: 5
                MAX_WALL_TO_LOSE: 0.2
        credentials : dict
            "API_KEY": 'xxx'
            "API_SECRET": 'xxx'
            Api key/secret combo needed for the exchange.

        Returns
        -------
        dict
            An object with callable buy and sell function to execute
            the dynamic_wall algo with the provided settings.
        """
        # Config Settings
        self.exchange_name = settings["EXCHANGE_NAME"]
        self.order_type = settings["ORDER_TYPE"]
        self.limit_price = settings["LIMIT_PRICE"]
        self.brick_in_base = settings["TOTAL_VOLUME_IN_BASE"]
        self.base_currency = settings["BASE_CURRENCY"]
        self.quote_currency = settings["QUOTE_CURRENCY"]
        self.sleep_time = settings["REFRESH_INTERVAL"]
        self.time_bound = settings['TIME_BOUND']
        self.distance = settings['DISTANCE_FROM_FAIR_PRICE']
        self.gap = settings['ORDER_GAP']
        self.slice_number = settings['SLICE_NUMBER']
        self.max_wall_to_lose = settings['MAX_WALL_TO_LOSE']
        self.price_change_threshold = settings['PRICE_CHANGE_THRESHOLD']
        self.collection = db['bot_trade_session']
        self.session_id = session_id
        self.total_traded_base = 0
        self.total_traded_quote = 0
        self.slack_webhook = settings['SLACK_LINK']
        self.algo_info = {
            'exchange_name': self.exchange_name, 
            'algo_name': 'dynamic_wall'}
        self.API = ExchangeAPI(session_id, self.algo_info, credentials, db)
        # API Settings
        precision_pulled = 0
        while not precision_pulled:
            try:
                precision = self.API.get_precision(self.base_currency, self.quote_currency)
                precision_pulled = 1
            except Exception as err:
                print('Failed to pull precision: %s ' % err)
        self.precision = precision['quantity_precision_in_quote']

    def run(self):
        """
        This function is common across all algos. It depends on the type of execution
        the algo should do based on the config.yaml file passed in.
        All functions should supported should be placed here into if statements and call
        the respective function within the class.

        Class Methods called
        --------------------
            buy() -> if self.type == buy
            sell() -> if self.type == sell
        """
        timestr = datetime.datetime.fromtimestamp(int(time.time())).strftime('%Y-%m-%d %H:%M:%S')
        cpprint('***** STARTING TIME: %s *****' % timestr)
        self.build_wall()
        self.update_endtime()

    def build_wall(self):
        """
        This is a function that build the wall.
        """
        # end_time is defined to terminate the program after certain
        # 
        end_time = int(time.time())+self.time_bound
        order_list = []
        legacy_price = 0
        price = 0
        while time.time()<end_time:
            price = self.get_edge_price(price)
            price_change = abs((price - legacy_price)/price)
            # The following function calculates the orders we will need to place.
            order_to_place = helpers.order_calculator(
                self.brick_in_base, 
                price, 
                self.limit_price, 
                self.order_type, 
                self.distance, 
                self.gap, 
                self.precision,
                self.slice_number)
            try:
                if price_change >= self.price_change_threshold:
                    # The following if statement will check if the order_list is empty
                    # or not, if it is empty which implies the program has just started.
                    # It will also check if our wall has been hit or not, if it is hit by 
                    # over 20% of total notional, the program will terminate.
                    if self.check_if_the_wall_is_hit(order_list):
                        order_list = self.cancel_past_orders(order_list)

                    order_list = self.place_wall_orders(order_to_place, order_list)
                    legacy_price = price
                else:
                    # The wall will remain unmoved if not too much change has happened in price.
                    print('Price did not move much, wall remains.')
                time.sleep(self.sleep_time)
            except KeyboardInterrupt:
                order_list = self.cancel_past_orders(order_list)
                self.update_endtime()
                print("All orders cancelled.")
                sys.exit()
            except JSONDecodeError:
                print('Failed to get data due to error data sent from api.')
                time.sleep(self.sleep_time)
            except KeyError:
                print('Failed to place order due to error data sent from api.')
                time.sleep(self.sleep_time)
            # handle other exceptions such as API timeouts or exceptions,
            # ensure to print it out but keep the algo going
            except Exception as err:
                print("EXCEPTION: " + str(err))
                print(traceback.format_exc())
                order_list = self.cancel_past_orders(order_list)
                time.sleep(10)
        order_list = self.cancel_past_orders(order_list)

    def get_edge_price(self, price):
        """
            This function is used to pull the last traded price of a currency pair.
        """
        try:
            edge_orders = self.API.get_orderbook(self.base_currency, self.quote_currency, 1)
            if self.order_type == 'sell':
                price = edge_orders['asks'][0]['price']
            elif self.order_type == 'buy':
                price = edge_orders['bids'][0]['price']
        except Exception as err:
            timestr = datetime.datetime.fromtimestamp(int(time.time())).strftime('%Y-%m-%d %H:%M:%S')
            print('%s: Failed to pull price: %s ' % (timestr, err))
        return price

    def check_if_the_wall_is_hit(self, order_list):
        """
            This function is built for sanity check. It check two cases:
            If the number of orders is 0, it will return False and tell the main program that the 
            wall builder has not initiated yet. 
            If there are orders in the orderlist, it will calculate how much is filled in this orders
            and log that information, if the filled amount is more than 20% if the entir order placed 
            to the market, the program will terminate.
        """
        if not order_list:
            print('Wall initiated')
            return False
        # The traded amount will be added to a global variable called self.total_traded_base
        for order in order_list:
            order_detail = self.API.get_order_details(
                self.base_currency, 
                self.quote_currency, 
                order, 
                self.order_type)
            self.total_traded_base = order_detail['filled_quantity_in_base']+self.total_traded_base
            self.total_traded_quote = order_detail['filled_quantity_in_quote']+self.total_traded_base
        print('%s %s has been taken in the last %s seconds.' 
                % (str(self.total_traded_base), self.base_currency, str(self.sleep_time)))
        print('We %s %s %s in the process.' % (self.order_type, self.total_traded_base, self.quote_currency))
        
        # If 
        if self.total_traded_base > self.brick_in_base * self.max_wall_to_lose:
            printout = "Fatal: Significant percentage of the order volume has been take, terminate the bot."
            printout = printout + '/n Session id: ' + self.session_id
            self.cancel_past_orders(order_list)
            print(printout)
            self.send_slack_message(printout)
            sys.exit()
        return True

    def cancel_past_orders(self, order_list):
        """
        Will get all orders form recorded orderlist cancelled.
        Parameters
        ----------
        order_list: list, list of place orders
        
        Return
        ------
        an empty list if successfully canceled.
        """
        for i in range(len(order_list)):
            order_cancelled = 0
            while not order_cancelled:
                try:
                    if self.API.cancel_order(
                        self.base_currency, 
                        self.quote_currency, 
                        order_list[0], 
                        self.order_type):
                        print('%s: %s cancelled' % (str(i), order_list[0]))
                        order_cancelled = 1
                except json.decoder.JSONDecodeError:
                    print('Failed to cancel order %s because of abnormal json reply, will retry after 10s.' % order_list[0])
                    time.sleep(10)
            order_list.remove(order_list[0])
        return order_list

    def place_wall_orders(self, order_to_place, order_list):
        """
            This function is used to execute the orders to set the wall
            from give order information.
        """
        for orders in order_to_place:
            order_id = self.API.place_order(
                self.base_currency, 
                self.quote_currency, 
                orders['order_type'], 
                orders['price'], 
                orders['size_in_quote'])
            order_list.append(order_id)
        return order_list

    def update_endtime(self):
        """
            This function is used to update the end time in trade session logger
            upon termination of the bot
        """
        end_timestr = datetime.datetime.fromtimestamp(int(time.time())).strftime('%Y-%m-%d %H:%M:%S')
        time_report = 'The bot with session id %s has stopped at %s .' % (self.session_id ,end_timestr)
        if self.order_type == 'buy':
            trade_report = 'We have used %s %s in this session to buy %s %s.' % (self.total_traded_base, self.base_currency, self.total_traded_quote, self.quote_currency)
        elif self.order_type == 'sell':
            trade_report = 'We have bought %s %s by selling %s %s.' % (self.total_traded_base, self.base_currency, self.total_traded_quote, self.quote_currency)
        self.send_slack_message(time_report)
        self.send_slack_message(trade_report)
        self.collection.find_one_and_update({'_id': self.session_id}, {'$set': {'end_time': end_timestr}})

    def send_slack_message(self, text):
        slk.send_message_to_slack(self.slack_webhook, text)
