import sys
import traceback
import time
import datetime
import json
import algos.twap.helpers as helpers
import utils.slack as slk
from api.exchange_api import ExchangeAPI
from prettyprinter import cpprint
from json.decoder import JSONDecodeError

class Twap:
    """
    Read Notion docs for specifications of this algorithm.
    The __init__ function contains further docs.
    """
    def __init__(self, settings, credentials, db, session_id):
        """
        Runs the TWAP algorithm with the following settings provided in
        the config.yaml file.

        Standard functions supported:
            >> twap_buy()
            >> twap_sell()

        Parameters
        ----------
        settings : dict
            ICEBERG:
                EXCHANGE_NAME: KUCOIN
                ORDER_TYPE: buy/sell
                QUANTITY_IN_QUOTE: 0 for buy order
				QUANTITY_IN_BASE: 0 for sell order 
                BASE_CURRENCY: btc
                QUORE_CURRENCY: eth
                CLIENT_NAME: DRAPER
                DURATION: 18000
				EXECUTION_INTERVAL: 300
        credentials : dict
            "API_KEY": 'xxx'
            "API_SECRET": 'xxx'
            Api key/secret combo needed for the exchange.

        Returns
        -------
        dict
            An object with callable buy and sell function to execute
            the twap algo with the provided settings.
        """
        # Config Settings
        self.order_book_depth = 20 # change as needed, typically 50 supported by most exchanges
        self.exchange_name = settings["EXCHANGE_NAME"]
        self.order_type = settings["ORDER_TYPE"]
        self.quantity_in_quote = settings["QUANTITY_IN_QUOTE"]
        self.quantity_in_base = settings["QUANTITY_IN_BASE"]
        self.base_currency = settings["BASE_CURRENCY"]
        self.quote_currency = settings["QUOTE_CURRENCY"]
        self.duration = settings["DURATION"]
        self.execution_interval = settings["EXECUTION_INTERVAL"]
        self.slack_webhook = settings['SLACK_LINK']
        self.collection = db['bot_trade_session']
        self.session_id = session_id
        self.algo_info = {
            'exchange_name': self.exchange_name, 
            'algo_name': 'twap'}
        # db_collection = db["twap"]
        self.API = ExchangeAPI(session_id, self.algo_info, credentials, db)
        # API Settings
        self.start_quote_balance = self.API.get_balance(self.quote_currency)['available_balance']
        self.start_base_balance = self.API.get_balance(self.base_currency)['available_balance']
        # Runtime settings
        if self.order_type == "buy":
            self.target_base_balance = self.start_base_balance - self.quantity_in_base
        if self.order_type == "sell":
            self.target_quote_balance = self.start_quote_balance - self.quantity_in_quote
        self.precision_quote = self.API.get_precision(self.base_currency, self.quote_currency)['quantity_precision_in_quote']

    def run(self):
        """
        This function is common across all algos. It depends on the type of execution
        the algo should do based on the config.yaml file passed in.
        All functions should supported should be placed here into if statements and call
        the respective function within the class.

        Class Methods called
        --------------------
            buy() -> if self.type == buy
            sell() -> if self.type == sell
        """
        timestr = datetime.datetime.fromtimestamp(int(time.time())).strftime('%Y-%m-%d %H:%M:%S')
        cpprint('***** STARTING TIME: %s *****' % timestr)
        self.twap_trade()
        self.update_endtime()

    def twap_trade(self):
        """
        Will keep selling token in limit chunks until provided volume is sold.

        The way of selling is that we sell an equivalent amount to the market at
        a fixe time interval within a duration, regardless of the price. In essence, 
        what this function does is to continuously place a market order of the same
        quantity.

        Exchange API calls
        --------------------
            get_orderbook()
            get_balance()

        Class Variables Used
        --------------------
            self.duration 
            self.execution_interval
        Class Methods called
        --------------------
            self.order_analyser
            self.cancel_place_order
            self.place_order
        """ 
        init_time = int(time.time())
        execution_timestamp_list = helpers.get_timestamp_list(
            init_time, self.duration, self.execution_interval)
        number_of_trades_left = len(execution_timestamp_list)
        for t in execution_timestamp_list:
            order_executed = False
            try:
                while time.time()<t:
                    if order_executed == False:
                        if t != execution_timestamp_list[0]:
                            self.cancel_placed_order(order_id)

                        order_info = self.order_analyser(number_of_trades_left)
                        # Execute the trade and store the order_id
                        order_id = self.place_order(order_info)
                        number_of_trades_left = number_of_trades_left-1
                        order_executed = True
                        running_time = int(time.time())-init_time
                        print('The bot has been running for %s seconds' % str(running_time))
            except KeyboardInterrupt:
                self.cancel_placed_order(order_id)
                self.update_endtime()
                print('Program manually interrupted.')
                sys.exit()
            except JSONDecodeError:
                print('Failed to get data due to error data sent from api.')
                time.sleep(10)
            except KeyError:
                print('Failed to place order due to error data sent from api.')
                time.sleep(10)
            except Exception as err:
                print('Error: %s' % err)
                self.send_slack_message('Fatal error happend during the trading: %s' % self.session_id)
                self.update_endtime()
                sys.exit()



    def order_analyser(self, number_of_trades_left):
        """
            This is a helper function to calculate the quantity and price for next order.
            Exchange API Calls
            -------------------
                API.get_balance()
                API.get_orderbook()
            Class vairable used
            -------------------
                self.API
                self.order_book_depth
                self.order_type
                self.base_currency 
                self.quote_currency
                self.target_quote_balance
                self.precision_quote
        """
        # Fetch orderbook untill it passes.
        orderbook_not_pulled = 1
        while orderbook_not_pulled:
            try:
                orderbook_info = self.API.get_orderbook(
                    self.base_currency, 
                    self.quote_currency, 
                    self.order_book_depth)
                orderbook_not_pulled = 0
            except Exception as err:
                print('Failed to pull orderbook information: %s ' % err)
                print(traceback.format_exc())
        # Calculate the price and execution quantity
        if self.order_type == 'sell':
            # get current balance of quote_currency
            current_quote_balance = self.API.get_balance(self.quote_currency)['available_balance']
            # get the amount left to trade buy measuring the distance between current balance and target balance
            amount_left_to_trade = current_quote_balance-self.target_quote_balance
            # print in command windows the amount we have left to trade.
            print('We have %s %s left to trade.' % (str(amount_left_to_trade), self.quote_currency))
            # calculate the amount to trade in next time interval
            quantity_to_sell_in_quote = round(amount_left_to_trade/number_of_trades_left, self.precision_quote)
            # use the helper function to calculate the exact trade numbers
            order_info = helpers.sell_market_calculator(
                quantity_to_sell_in_quote, 
                orderbook_info['bids'])
        elif self.order_type == 'buy':
            current_base_balance = self.API.get_balance(self.base_currency)['available_balance']
            amount_left_to_trade = current_base_balance-self.target_base_balance
            print('We have %s %s left to trade.' % (str(amount_left_to_trade), self.base_currency))
            quantity_to_buy_in_base = round(amount_left_to_trade/number_of_trades_left, self.precision_quote)
            order_info = helpers.buy_market_calculator(
                quantity_to_buy_in_base, 
                orderbook_info['asks'])
        return order_info

    def cancel_placed_order(self, order_id):
        """
        This function is used to cancel an order give a specific order id.

        Exchange API calls
        --------------------
            API.cancel_order()

        Class Variables Used
        --------------------
            self.base_currency 
            self.quote_currency
            self.order_type
        """
        self.API.cancel_order(
            self.base_currency, 
            self.quote_currency, 
            order_id, 
            self.order_type)
    
    def place_order(self, order_info):
        """
        This function is used to place an order give specific order information.

        Parameter
        ---------
        order_info: dict {'price': float, 'quantity': float}

        Exchange API calls
        --------------------
            API.place_order()

        Class Variables Used
        --------------------
            self.base_currency 
            self.quote_currency
            self.order_type
        """
        order_id = self.API.place_order(
            self.base_currency, 
            self.quote_currency, 
            self.order_type, 
            order_info['price'], 
            order_info['quantity'])
        return order_id

    def update_endtime(self):
        """
            This function is used to update the end time in trade session logger
            upon termination of the bot
        """
        end_timestr = datetime.datetime.fromtimestamp(int(time.time())).strftime('%Y-%m-%d %H:%M:%S')
        slack_report = 'The bot with session id %s has stopped at %s .' % (self.session_id ,end_timestr)
        self.send_slack_message(slack_report)
        self.collection.find_one_and_update({'_id': self.session_id}, {'$set': {'end_time': end_timestr}})

    def send_slack_message(self, text):
        slk.send_message_to_slack(self.slack_webhook, text)
