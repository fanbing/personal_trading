def buy_market_calculator(required_quantity_in_base, ask_orders):
    """
    This function will place a limit order at a price where the required quantity 
    happens to be completely filled.

        Parameters
        -----------
        required_quantity_in_base: float
        ask_orders: list  [{'price': float, 'size_in_quote': float, 'size_in_quantity': float}]

        Returns
        --------
        list
        [price, quantity_to_execute_in_quote]: [float, float]

    """

    # Firstly we need to check if the orderbook is thick enough for us to place order.
    # Currently the requirement is pretty low, as long as there is enough counter orders, we 
    # can place order. Later on we may want to set a limit price, if there is not enough order
    # in our favor, we place smaller orders.
    total_ask_base = 0
    for i in range(len(ask_orders)):
        total_ask_base = total_ask_base + ask_orders[i]['size_in_base']
    if total_ask_base < required_quantity_in_base:
        print('Orderbook too thin to place limit order.')
        return False

    available_quantity = 0
    executed_quote = 0
    executed_base = 0
    index = 0
    # The following loop is used to calculate price to place
    while available_quantity < required_quantity_in_base:
        available_quantity = available_quantity+ask_orders[index]['size_in_base']
        index = index+1
    price = ask_orders[index-1]['price']
    # The following calculation is used to calculate the quote quantity to buy. This is 
    # unique for buying because usually we are asked to use certain base currency to buy but the
    # returned value of orderbook info will be in quote. So we will need conversion here. Therefore,
    # the following step is not necessary for sell order.
    if index >= 2:
        for i in range(index-2):
            executed_quote = executed_quote + ask_orders[i]['size_in_quote']
            executed_base = executed_base + ask_orders[i]['size_in_base']
    quantity_to_execute_in_quote = executed_quote + (required_quantity_in_base - executed_base)/price
    return {'price': price, 'quantity': quantity_to_execute_in_quote}

def sell_market_calculator(required_quantity_in_quote, bid_orders):
    """
    This function will place a limit order at a price where the required quantity 
    happens to be completely filled.

        Parameters
        -----------
        required_quantity_in_base: float
        bis_orders: list  [{'price': float, 'size_in_quote': float, 'size_in_quantity': float}]

        Returns
        --------
        list
        [price, quantity_to_execute_in_quote]: [float, float]
        
    """
    # Check order thickness as above.
    total_bid_quote = 0
    for i in range(len(bid_orders)):
        total_bid_quote = total_bid_quote + bid_orders[i]['size_in_quote']
    if total_bid_quote < required_quantity_in_quote:
        print('Orderbook too thin to place limit order.')
        return False

    available_quantity = 0
    index = 0
    # To calculate price.
    while available_quantity < required_quantity_in_quote:
        available_quantity = available_quantity+bid_orders[index]['size_in_quote']
        index = index+1
    price = bid_orders[index-1]['price']
    return {'price': price, 'quantity': required_quantity_in_quote}
    
def get_timestamp_list(init_time, duration, time_interval):
    """
        Two requirements for this function.
        1. len(time_list) == duration/time_interval
        2. time.time() < init_time+1
    """
    time_list = range(init_time+1, init_time+duration+1, time_interval)
    return time_list