import datetime
import sys
import time
import traceback
import json
import algos.iceberg.helpers as helpers
import utils.slack as slk
from prettyprinter import cpprint
from json.decoder import JSONDecodeError
from api.exchange_api import ExchangeAPI


class Iceberg:
    """
    Read Notion docs for specifications of this algorithm.
    The __init__ function contains further docs.
    """
    def __init__(self, settings, credentials, db, session_id):
        """
        Runs the Iceberg algorithm with the following settings provided in
        the config.yaml file.

        Standard functions supported:
            >> buy()
            >> sell()

        Parameters
        ----------
        settings : dict
            ICEBERG:
                EXCHANGE_NAME: KUCOIN
                CLIENT_NAME: DRAPER
                LIMIT_PRICE: 0.06
                TOTAL_VOLUME: 0.017
                MAX_ORDER_SIZE: 0.001
                ALT_ORDER_SIZE: 0.001
                TYPE: BUY
                QUOTE_CURRENCY: ETH
                BASE_CURRENCY: BTC
                SLEEP_TIME: 10
                TIME_BOUND: 86400
        credentials : dict
            "API_KEY": 'xxx'
            "API_SECRET": 'xxx'
            Api key/secret combo needed for the exchange.

        Returns
        -------
        dict
            An object with callable buy and sell function to execute
            the iceberg algo with the provided settings.
        """
        # Config Settings
        self.order_book_depth = 50 # change as needed, typically 50 supported by most exchanges
        self.exchange_name = settings["EXCHANGE_NAME"]
        self.type = settings["TYPE"]
        self.limit_price = settings["LIMIT_PRICE"]
        self.total_volume = settings["TOTAL_VOLUME"]
        self.max_order_size = settings["MAX_ORDER_SIZE"]
        self.alt_order_size = settings["ALT_ORDER_SIZE"]
        self.aggressive = settings['AGGRESSIVE']
        self.token = settings["QUOTE_CURRENCY"]
        self.base = settings["BASE_CURRENCY"]
        self.sleep_time = settings["SLEEP_TIME"]
        self.time_bound = settings['TIME_BOUND']
        self.slack_webhook = settings['SLACK_LINK']
        self.symbol = self.token + '-' + self.base
        self.amount_executed = 0
        self.collection = db['bot_trade_session']
        self.session_id = session_id
        self.algo_info = {
            'exchange_name': self.exchange_name, 
            'algo_name': 'iceberg'}
        self.API = ExchangeAPI(session_id, self.algo_info, credentials, db)
        # API Settings
        self.start_token_balance = self.API.get_balance(self.token)['available_balance']
        # Runtime settings
        if self.type == "buy":
            self.needed_token_balance = self.start_token_balance + self.total_volume
        if self.type == "sell":
            self.needed_token_balance = self.start_token_balance - self.total_volume
        self.precision = self.API.get_precision(self.base, self.token)['quantity_precision_in_quote']

    def run(self):
        """
        This function is common across all algos. It depends on the type of execution
        the algo should do based on the config.yaml file passed in.
        All functions should supported should be placed here into if statements and call
        the respective function within the class.

        Class Methods called
        --------------------
            buy() -> if self.type == buy
            sell() -> if self.type == sell
        """
        timestr = datetime.datetime.fromtimestamp(int(time.time())).strftime('%Y-%m-%d %H:%M:%S')
        cpprint('***** STARTING TIME: %s *****' % timestr)
        if self.type == "buy":
            self.buy()
        if self.type == "sell":
            self.sell()
        self.update_endtime()

    def sell(self):
        """
        Will keep selling token in limit chunks until provided volume is sold.

        Calculated current_coin_balance and keeps going until the needed token balance
        is reached. Will sell with limit every iteration and check the coin balance,
        adjust for how much of the order placed was filled and update the current balance
        accordingly.

        Exchange API calls
        --------------------
            get_orderbook()
            get_open_order()
            get_balance()

        Class Variables Used
        --------------------
            self.start_token_balance
            self.precision
            self.needed_token_balance
            self.base
            self.token
            self.order_book_depth
            self.limit_price
            self.sleep_time

        Class Methods called
        --------------------
            order_with_limit()
        """
        current_coin_balance = round(self.start_token_balance, self.precision)
        init_time = int(time.time())
        while current_coin_balance > self.needed_token_balance:
            if time.time() > init_time + self.time_bound:
                helpers.cancel_all_orders(self.base, self.token, self.API, 'sell')
                print("Time is up for the algo, all orders cancelled.")
                return True
            # try selling, if error check logs
            try:
                # grab the current asks and any open orders
                current_bids = self.API.get_orderbook(self.base, self.token, self.order_book_depth)['bids']
                # get open orders in dict format (last arg = true)
                current_open_orders = self.API.get_open_order(self.base, self.token, True)['sell']
                # current - needed will give remaninder to sell
                remaining_balance_needed = current_coin_balance - self.needed_token_balance

                self.order_with_limit(
                    self.type,
                    self.base,
                    self.token,
                    current_bids,
                    current_open_orders,
                    self.limit_price,
                    self.max_order_size,
                    self.alt_order_size,
                    remaining_balance_needed
                    )
                # check the coin balance to see how much was filled, round, and print it.
                # this balance is updated and checked at the top of the loop
                # to see if have reached the needed_token_balance
                balance = self.API.get_balance(self.token)
                current_coin_balance = balance['available_balance']+balance['reserved_balance']
                current_coin_balance = round(current_coin_balance, self.precision)
                print("\tEnding coin balance: " + str(current_coin_balance))
                # sleep before repeating to avoid timeouts and allow orderbook to update
                self.amount_executed = self.start_token_balance - current_coin_balance
                cpprint('We have sold %s %s' % (str(self.amount_executed), self.token))
                time.sleep(2)
                # helpers.cancel_all_orders(self.base, self.token, self.API, 'sell')
            # exit out of program on manual exit (Cntrl c)
            except KeyboardInterrupt:
                helpers.cancel_all_orders(self.base, self.token, self.API, 'sell')
                print("All orders cancelled.")
                self.update_endtime()
                sys.exit()
            except JSONDecodeError:
                print('Failed to place order due to error data sent from api. The program will continue.')
                time.sleep(self.sleep_time)
            except KeyError:
                print('Failed to place order due to error data sent from api. The program will continue.')
                time.sleep(self.sleep_time)
            # handle other exceptions such as API timeouts or exceptions,
            # ensure to print it out but keep the algo going
            except Exception as err:
                print("EXCEPTION: " + str(err))
                print(traceback.format_exc())
                helpers.cancel_all_orders(self.base, self.token, self.API, 'sell')
                time.sleep(self.sleep_time)

    def buy(self):
        """
        Will keep buying token in limit chunks until provided volume is reached.

        Calculated current_coin_balance and keeps going until the needed token balance
        is reached. Will buy with limit every iteration and check the coin balance,
        adjust for how much of the order placed was filled and update the current balance
        accordingly.

        Exchange API calls
        --------------------
            get_orderbook()
            get_open_order()
            get_balance()

        Class Variables Used
        --------------------
            self.start_token_balance
            self.precision
            self.needed_token_balance
            self.base
            self.token
            self.order_book_depth
            self.limit_price
            self.sleep_time

        Class Methods called
        --------------------
            order_with_limit()
        """
        current_coin_balance = round(self.start_token_balance, self.precision)
        init_time = int(time.time())
        while current_coin_balance < self.needed_token_balance:
            if time.time() > init_time + self.time_bound:
                helpers.cancel_all_orders(self.base, self.token, self.API, 'buy')
                print("Time is up for the algo, all orders cancelled.")
                return True
            # try buying, if error check logs
            try:
                # grab the current asks and any open orders
                current_asks = self.API.get_orderbook(self.base, self.token, self.order_book_depth)['asks']
                # get open orders in dict format (last arg = true)
                current_open_orders = self.API.get_open_order(self.base, self.token, True)['sell']
                # buy based on iceberg logic, get the oid of the order placed
                # will place order regardless of volume, so expect an oid.
                remaining_balance_needed = self.needed_token_balance - current_coin_balance
                # if running low on base currency, take the max volume we can buy
                self.order_with_limit(
                    self.type,
                    self.base,
                    self.token,
                    current_asks,
                    current_open_orders,
                    self.limit_price,
                    self.max_order_size,
                    self.alt_order_size,
                    remaining_balance_needed,
                    )
                # check the coin balance to see how much was filled, round, and print it.
                # this balance is updated and checked at the top of the loop to see
                # if have reached the needed_token_balance
                balance = self.API.get_balance(self.token)
                current_coin_balance = balance['available_balance']+balance['reserved_balance']
                current_coin_balance = round(current_coin_balance, self.precision)
                print("\tEnding coin balance: " + str(current_coin_balance))
                # sleep before repeating to avoid timeouts and allow orderbook to update
                self.amount_executed = current_coin_balance - self.start_token_balance
                cpprint('We have bought %s %s' % (str(self.amount_executed), self.token))
                time.sleep(2)
                #helpers.cancel_all_orders(self.base, self.token, self.API, 'buy')
            # exit out of program on manual exit (Cntrl c)
            except KeyboardInterrupt:
                helpers.cancel_all_orders(self.base, self.token, self.API, 'buy')
                print("All orders cancelled.")
                self.update_endtime()
                sys.exit()
            # Handle the frequently pop out kucoin json error.
            except JSONDecodeError:
                print('Failed to place order due to error data sent from api. The program will continue.')
                time.sleep(self.sleep_time)
            except KeyError:
                print('Failed to place order due to error data sent from api. The program will continue.')
                time.sleep(self.sleep_time)
            # ensure to print it out but keep the algo going
            except Exception as err:
                helpers.cancel_all_orders(self.base, self.token, self.API, 'buy')
                print("EXCEPTION: " + str(err))
                print(traceback.format_exc())
                time.sleep(self.sleep_time)

    def order_with_limit(
            self,
            order_type, 
            base,
            token,
            current_orders,
            current_open_orders,
            limit_price,
            max_order_size,
            alt_order_size,
            remaining_balance_needed
        ):
        """
        Buys either a minimum amount of volume based on the max_order_size, the remaining tokens
        needed to reach the goal, the amount of base currency left, or the max volume available
        on the market that is below the limit price.

        Class Methods called
        --------------------
            place_order(base, token, limit_price, alt_order_size/final_volume)
        
        Parameters
        ----------
        order_type: string
            ['buy', 'sell']
        base: string
            The base currency in the trading pair
        token: string
            The quote currency in the trading pair
        current_orders: list 
            List of asks/sells in the order book
        current_open_orders:
            List of currently placed ask/bid orders
        limit_price: float
            The max price for what an order should be placed for 
        max_order_size: float
            The max volume of an order that should be placed
        alt_order_size: float
            If no volume is available, the minimum volume bid that should be placed at the limit price
            even if it does not get filled.
        remaining_balance_needed: float
            The difference between the current coin balance and the target amount we need to buy. 
            The target amount is determined at the start of execution by adding current balance to the 
            request total buy size.
        """
        # based on the current order book and limit price, get the amount of volume available to buy/sell
        order_status = helpers.get_volume_of_valid_orders(order_type, limit_price, current_orders)
        volume_available = order_status['volume']
        # if no volume is available and no orders are currently placed,
        # place a small bid (alt_order_size) at the limit price
        if volume_available == 0:                   #or not current_open_orders 
            oid = self.place_order(order_type, base, token, limit_price, alt_order_size)
            # wait for a few seconds to allow the exchange to fill the order
            time.sleep(self.sleep_time)
            self.API.cancel_order(base, token, oid, order_type)
            return oid
        # if volume is available, take the min of how much we need based on the vars below
        final_volume = min(
            max_order_size,
            remaining_balance_needed,
            volume_available
            )
        # place the order
        if self.aggressive:
            if self.type == 'buy':
                price_to_place = min([order_status['price']*1.05, self.limit_price])
            elif self.type == 'sell':
                price_to_place = max([order_status['price']/1.05, self.limit_price])
        else:
            price_to_place = order_status['price']
        oid = self.place_order(order_type, base, token, price_to_place, final_volume)
        # wait for a few seconds to allow the exchange to fill the order
        time.sleep(self.sleep_time)
        # cancel the order placed and go to next cycle
        # cancel_result = self.API.cancel_order(base, token, oid, order_type)
        self.API.cancel_order(base, token, oid, order_type)


    def place_order(self, order_type, base, token, limit_price, volume):
        """
        Places a buy or a sell order to the exchange that the algo is being
        run with.

        Parameters
        ----------
        order_type : string 
            ['buy','sell']
        base : string
            The base currency of the order being placed.
        token : string
            The quote currency of the order being placed.
        limit_price : float
            The price to put the order at on the market.
        volume:
            The amount of the quote/token currency being bought/sold.

        Returns
        -------
        string
            The order id of the placed order.
        """
        oid = self.API.place_order(
            base,
            token,
            order_type,
            limit_price,
            volume
            )
        return oid

    def update_endtime(self):
        """
            This function is used to update the end time in trade session logger
            upon termination of the bot
        """
        end_timestr = datetime.datetime.fromtimestamp(int(time.time())).strftime('%Y-%m-%d %H:%M:%S')
        time_report = 'The bot with session id %s has stopped at %s .' % (self.session_id ,end_timestr)
        trade_report = 'The bot has %s %s %s in this session.' % (self.type, str(self.amount_executed), self.token)
        self.send_slack_message(time_report)
        self.send_slack_message(trade_report)
        self.collection.find_one_and_update({'_id': self.session_id}, {'$set': {'end_time': end_timestr}})

    def send_slack_message(self, text):
        print(slk.send_message_to_slack(self.slack_webhook, text))
