
def get_volume_of_valid_orders(order_type, limit_price, orders):
    """
    Depending on a buy or sell, will get the volume of the bids/asks
    within the limit price provided from the order list.

    Parameters
    ----------
    order_type : string
        ['buy','sell']
    limit_price : float
        The price to put the order at on the market.
    orders: list
        Either bids or asks in the proper API format as
        per the ExchangeAPI get_orders() function.

    Returns
    -------
    list:
    [
    volume available: float
        The sum of the volume of the quote currency that is
        under/above the limit price depending on buy/sell.
    highest bid price: float
    lowest ask price: float
    ]
    """
    valid_orders = []
    price = orders[0]['price']
    if order_type == 'buy':
        valid_orders = list(map(
            lambda ask: ask['size_in_quote'] if ask['price'] <= limit_price else 0, orders))
    elif order_type == 'sell':
        valid_orders = list(map(
            lambda ask: ask['size_in_quote'] if ask['price'] >= limit_price else 0, orders))

    sum_of_quote_volumes = sum(ask for ask in valid_orders)
    #sum_of_quote_volumes = sum(valid_orders)
    return {'volume': sum_of_quote_volumes, 'price': price}

def cancel_all_orders(base, token, api, order_type):
    """
    Will get all current open orders using
    the api being used and cancel them all.

    Parameters
    ----------
    base: string
        The base currency in the trading pair
    token: string
        The quote currency in the trading pair
    api: object
        The api object being used by the current running
        algo.
    """
    print('cancelling orders')
    all_open_orders = api.get_open_order(base, token)
    print(all_open_orders)
    for order in all_open_orders:
        try: 
            cancelled = api.cancel_order(base, token, order['order_id'], order_type)
            if not cancelled:
                print(order['order_id'] + 'did not get cancelled')
        except Exception as err:
            print("EXCEPTION: " + str(err))
            print(order['order_id'] + 'did not get cancelled')
