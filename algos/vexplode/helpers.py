import random
import math
import time
import requests

def abundancy_calculator(
	coin_list,
	initial_balances, 
	current_balances):
	"""
		Description
		-----------
		This function is used to chose the most abundant currency
		within one account.

		Parameters
		----------
		coin_list: list
		current_balances: list
		
		return
		--------
		account_abundance: list
			[coin_name, coin_name]
	"""
	account_abundance = ['', '']
	for i in range(2):
		abundance = 1
		for currency in coin_list:
			bench_mark_balance = (initial_balances[0][currency]+initial_balances[1][currency])/2
			new_abundance = current_balances[i][currency]/bench_mark_balance
			if new_abundance >= abundance:
				account_abundance[i] = currency
				abundance = new_abundance
	return account_abundance

def skewness_calculator(
	coin_list, 
	initial_balances, 
	current_balances):
	"""
		Description
		-----------
		This function is used to calculate the skewness
		of each pair across two accounts.

		Parameters
		----------
		coin_list: list
		initial_balances: list
		current_balances: list
		
		return
		--------
		skewness: dict
	"""
	skewness = {}
	for coin in coin_list:
		bench_mark_balance = (initial_balances[0][coin]+initial_balances[1][coin])/2
		skewness[coin] = current_balances[0][coin]*current_balances[1][coin]/math.pow(bench_mark_balance, 2)
	return skewness

def skewness_to_size_cap(skewness_threshold, initial_balance):
	"""
		Description
		-----------
		Calculate the size cap for each transaction, this is a qualitative
		calculation.

		Parameters
		----------
		skewness_threshold: float
		initial_balance: list
		
		return
		--------
		size_cap: float
	"""
	size_cap = math.sqrt(skewness_threshold)*initial_balance/2
	return size_cap

def trade_size_calculator(
	volume_left_to_trade, 
	time_left, 
	average_trade_interval,
	size_cap
	):
	size = volume_left_to_trade/(time_left/(average_trade_interval+3))
	size = abs(random.normalvariate(size, size/2))
	if size > size_cap:
		size = size_cap
	return size

def price_calculator( 
	precision,
	la_price, 
	hb_price):
	min_unit = math.pow(10, -precision)
	freedom_interval = la_price-hb_price-min_unit*2
	if freedom_interval >= 0:
		random_element = round(freedom_interval*random.random(), precision)
		price = round(hb_price + min_unit + random_element, precision)
		return price
	else:
		print('Gap too narrow to place order')
		return False
