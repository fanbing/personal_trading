import sys
import traceback
import time
import datetime
import json
import random
from prettyprinter import cpprint
from api.exchange_api import ExchangeAPI
import algos.vexplode.helpers as hps
from json.decoder import JSONDecodeError
import utils.slack as slk


class Vexplode:
    """
        Overview:
            Dual-account vexplode is different from algos in two ways.

            1. It introduces two accounts while all the other algos we have
            developed so far are operating on only one account.

            2. It involves trading amount multiple pairs wheares our other 
            algos mostly focus on trading on pair.

            Because of these differences, our setup of the program is also going
            to be a bit different from the other algos. First of all, all the 
            global variables: credential, API, balances are now list of two items.
            I see them as list of two items instead of a dictionary with keys is 
            for the convenience of switching between two accounts. The trade-off of
            doing so is the difficulty in scaling up into more than 2 accounts and 
            some ambiguity in defining which account is which. 

            balances are of the format: [{coin1: float, coin2: float, coin3: float}]
            credentials: [{api_key: , api_secret: }, {api_key: , api_secret: }]
            API: [api with credential[0], api with credential[1]]

            Another thing is that there are three global variables that are related 
            to coin names. Since the algo will primarily focusing on exchanging under
            a fixed coin pair, there is base_currency and quote_currency. However, in
            scarcity event, it may require exchange with external currency. E.g. in an 
            account with cbc, btc and eth, while the bot is trading cbc-btc pair, when
            btc has become scarce in the account, the program should be able to exchange
            eth into btc. For such purpose, we has included another global varible 
            called coinlist, that can be thought as base_currency, quote_currency plus a
            set of other currencies the accounts are holding.

            Parameters
            ----------
            settings : dict
                VEXPLODE:
                    EXCHANGE_NAME: KUCOIN
                    CLIENT_NAME: DRAPER
                    COIN_LIST: 
                        - cbc
                        - btc
                        - eth
                    QUOTE_CURRENCY: cbc
                    BASE_CURRENCY: btc
                    USD_TARGET: 100000
                    TIME_BOUND: 86400
                    AVERAGE_TRADE_INTERVAL: 30
                    FREQUENCY_CHANGE_INTERVAL: 3600
                    SKEWNESS_THRESHOLD: 0.7
                    CURRENCY_HIERARCHY: 
                        cbc: 1
                        eth: 2
                        btc: 3
                    ACCOUNTS:
                        API_KEY:
                            - key of account 1
                            - key of account 2
                        API_SECRET:
                            - secret of account 1
                            - secret of account 2
                    SLACK_LINK: https://hooks.slack.com/services/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
        
        local functions
        ---------------
            High-level: 
                run(), 
                main_loop()
            Mid_level: 
                wash_trade(), 
                scarcity_event(), 
                rebalance_evenet()
            Low_level: 
                get_precision(), 
                get_current_balance(), 
                direction_calculator(), 
                place_market_order(), 
                place_limit_order(),
                cancel_existing_orders(),
                get_edge_prices(), 
                pdate_endtime(), 
                send_slack_message()

        helpers
        -------
            abundancy_calculator
            skewness_calculator
            skewness_to_size_cap
            trade_size_calculator
            price_calculator
    """
    def __init__(self, settings, db, session_id):
        """
            Set all the global variables, receive input data.
            Functions used:
            local
            -----
                self.get_precision()
                self.get_current_balance()
                self.update_quote_target()

            helpers
            -------
                hps.skewness_to_size_cap()
            
        """
        self.coin_list = settings['COIN_LIST']
        self.base_currency = settings['BASE_CURRENCY']
        self.quote_currency = settings['QUOTE_CURRENCY']
        self.currency_hierarchy = settings['CURRENCY_HIERARCHY']
        self.usd_target = settings['TAEGET_IN_USD']
        self.average_trade_interval = settings['AVERAGE_TRADE_INTERVAL']
        self.sleep_time = self.average_trade_interval
        self.frequency_change_interval = settings['FREQUENCY_CHANGE_INTERVAL']
        self.initial_time = int(time.time())
        self.end_time = self.initial_time+settings['TIME_BOUND']
        self.skewness_threshold = settings['SKEWNESS_THRESHOLD']
        self.collection = db['bot_trade_session']
        self.slack_webhook = settings['SLACK_LINK']
        self.session_id = session_id
        self.quote_volume_traded = 0
        self.usd_volume_trade = 0
        self.credentials = settings['ACCOUNTS']
        self.event_counter = {'scarcity': 0, 'rebalance': 0}
        self.order_info = []
        self.precision = {}       
        self.flow_direction = 0
        self.API = []
        self.balances = []
        self.quote_target = 0
        algo_info = {'exchange_name': settings['EXCHANGE_NAME'],
                    'algo_name': 'VEXPLODE'}
        for i in range(2):
            credential = {'API_KEY': self.credentials['API_KEY'][i], 
                          'API_SECRET': self.credentials['API_SECRET'][i]}
            self.API.append(ExchangeAPI(session_id, algo_info, credential, db))
        self.update_quote_target()
        self.get_precision() 
        self.initial_balances = self.get_current_balance()
        self.size_cap = hps.skewness_to_size_cap(
            self.skewness_threshold, 
            self.initial_balances[0][self.quote_currency])

    def run(self):
        """
            This is the function that is going to be called in the main.py in 
            root directory.
            Functions used:
            local
            -----
                self.main_loop()
                self.update_endtime()

        """
        timestr = datetime.datetime.fromtimestamp(int(time.time())).strftime('%Y-%m-%d %H:%M:%S')
        cpprint('***** STARTING TIME: %s *****' % timestr)
        self.main_loop()
        self.update_endtime()
    
    def main_loop(self):
        """
            Description:
            This function is a while loop that will not stop until the 
            desired endtime has reached.

            Functions used:
            local
            -----
                self.update_quote_target()
                self.get_current_balance()
                self.ideal_event()
                self.scarcity_event()
                self.rebalance_event()

            helpers
            -------
                hps.skewness_calculator()
                hps.trade_size_calculator()

            Parameters
            ----------
                None
            
            Return
            -------
                None
        """
        while time.time() < self.end_time:
            try:
                self.cancel_existing_orders()
                self.update_quote_target()
                current_balances = self.get_current_balance()
                skewness = hps.skewness_calculator(
                    self.coin_list, 
                    self.initial_balances, 
                    current_balances)
                skewed_coin = min(skewness)
                time_left = self.end_time - int(time.time())
                if skewness[skewed_coin] > self.skewness_threshold:
                    self.ideal_event(time_left)
                else:
                    initial_total_balance = self.initial_balances[0][skewed_coin] + self.initial_balances[1][skewed_coin]
                    skewed_total_balance = current_balances[0][skewed_coin] + current_balances[1][skewed_coin]
                    if skewed_total_balance < self.skewness_threshold * initial_total_balance:
                        self.scarcity_event(skewed_coin ,current_balances)
                    else:
                        self.rebalance_event(skewed_coin, current_balances, time_left)
                randomized_sleep_time = self.sleep_time_randomizer()
                time.sleep(randomized_sleep_time)
            except KeyboardInterrupt:
                self.cancel_existing_orders()
                print("All orders cancelled.")
                self.update_endtime()
                sys.exit()
            except JSONDecodeError:
                print('Failed to place order due to error data sent from api. The program will continue.')
                time.sleep(10)
            except KeyError:
                print('Failed to place order due to error data sent from api. The program will continue.')
                time.sleep(10)
            # handle other exceptions such as API timeouts or exceptions,
            # ensure to print it out but keep the algo going
            except Exception as err:
                print("EXCEPTION: " + str(err))
                print(traceback.format_exc())
                self.cancel_existing_orders()
                sys.exit()

    def ideal_event(self, time_left):             # tested
        """
            Description:
            This function execute an normal event where no skewed 
            pair has been detected, it will simply find the mid price 
            and execute a wash trade.

            Functions used:
            local
            -----
                self.direction_calculator()

            helpers
            -------
                hps.trade_size_calculator()

            Parameters
            ----------
                time_left: int (the amount of time left until the end of time bound)
            Return
            -------
                order_id: list  [string, string]
        """
        buy_account = self.direction_calculator()
        quantity = hps.trade_size_calculator(
            self.quote_target, 
            time_left, 
            self.average_trade_interval, 
            self.size_cap)
        self.wash_trade(buy_account, quantity)

    def scarcity_event(self, scarce_currency, current_balances):     
        """
            Description:
            This function execute an scarcity event.
            Find the abundancy coin of each account, sell the abundancy
            coin to scarce currency to the market.

            Functions used:
            local
            -----
                self.place_market_order()

            helpers
            -------
                hps.abundancy_calculator()

            Parameters
            ----------
                scarce_currency: string (e.g 'cbc', 'eth')
                current_balances: list  (format see class description )
            Return
            -------
                order_id: list  [string, string]
        """
        cpprint('Scarcity event triggered.')
        if self.event_counter['scarcity'] >= 10:
            print('Bot shut down because of too many scarcity happend.')
        abundance = hps.abundancy_calculator(self.coin_list, self.initial_balances, current_balances)
        order_id = []
        
        for i in range(2):
            abundant_currency = abundance[i]
            quantity = 0.9*(self.initial_balances[i][scarce_currency]-current_balances[i][scarce_currency])
            if quantity > 0:
                if self.currency_hierarchy[scarce_currency] > self.currency_hierarchy[current_balances]:
                    base = scarce_currency
                    quote = abundant_currency
                    order_side = 'sell'
                else:
                    quote = scarce_currency
                    base = abundant_currency
                    order_side = 'buy'
                order_id.append(self.place_market_order(i, base, quote, order_side, quantity))
        
        return True

    def rebalance_event(self, unbalanced_currency, current_balances, time_left):     # tested
        """
            Description:
            This function execute an rebalance event. I will
            take the most unbalanced currency, measure which account
            is overflowed and which account is deficit. Then make a
            trade to sell quote currency from overflowed account to 
            deficit account.

            Functions used:
            local
            -----
                self.wash_trade()

            helpers
            -------
                hps.trade_size_calculator()

            Parameters
            ----------
                scarce_currency: string (e.g 'cbc', 'eth')
                current_balances: list  (format see class description )
            Return
            -------
                True or False
        """
        cpprint('Rebalance event is triggered due to unbalance of %s.' % unbalanced_currency)
        quantity = hps.trade_size_calculator(
                    self.quote_target, 
	                time_left, 
	                self.average_trade_interval, 
	                self.size_cap)
        if unbalanced_currency == self.quote_currency:
            if current_balances[0][self.quote_currency] >= current_balances[1][self.quote_currency]:
                self.wash_trade(1, quantity)
            else:
                self.wash_trade(0, quantity)
            return True
        elif unbalanced_currency == self.base_currency:
            if current_balances[0][self.base_currency] >= current_balances[1][self.base_currency]:
                self.wash_trade(0, quantity)
            else:
                self.wash_trade(1, quantity)
            return True
        else:
            print('Un-balance happened in another currency, this bot will not deal with it.')
            return False

    def wash_trade(self, buy_account, quantity):       # tested
        """
            Description:
            This function executes a wash trade. It needs the buying account 
            fixed. Then it will first calculate a mid price, if the spread is 
            too narrow, it will not execute any order. Than it will random decide
            the sequence of buy and sell.

            Functions used:
            local
            -----
                self.get_edge_prices()
                self.place_limit_order()

            helpers
            -------
                hps.price_calculator()

            Parameters
            ----------
                buy_account: int (0 or 1)
                quantity: float  (the amount of quote_currency to execute)
            Return
            -------
                True of False
        """
        edge_prices = self.get_edge_prices(self.base_currency, self.quote_currency)
        la_price = edge_prices['lowest_ask']
        hb_price = edge_prices['highest_bid']
        sell_account = 1 - buy_account
        price = hps.price_calculator(self.precision[self.base_currency], la_price, hb_price)
        if price:
            green_or_red = random.random()
            if green_or_red > 0.5:
                self.place_limit_order(sell_account, 'sell', price, quantity)
                self.place_limit_order(buy_account, 'buy', price, quantity)
            else:
                self.place_limit_order(buy_account, 'buy', price, quantity)
                self.place_limit_order(sell_account, 'sell', price, quantity)
            self.quote_volume_traded = self.quote_volume_traded + quantity
        else:
            print('Spread too narrow to place order.')
            return False

    def get_precision(self):    # tested
        """
            Description:
            This function is called to define the global varible self.precision
            which attach an integer to each member in the self.coin_list that 
            restrict the max number of decimal a currency quantity can have.

            Functions used:
            API
            -----
                API.get_precision
            Parameters
            ----------
                None
            Return
            -------
                None
        """
        for currency in self.coin_list:
            if currency == self.quote_currency:
                precision = self.API[0].get_precision(self.base_currency, currency)['quantity_precision_in_quote']
            else:
                precision = self.API[0].get_precision(currency, self.quote_currency)['price_precision']
            self.precision[currency] = precision

    def get_current_balance(self):    # tested
        """
            Description:
            This function is called to get the balances of coins in 
            two accounts.

            Functions used:
            API
            -----
                API.get_balance
            Parameters
            ----------
                None
            Return
            -------
                balances: list  (format see class description)
        """
        balances = [{}, {}]
        for i in range(2):
            for coin in self.coin_list:
                balance = self.API[i].get_balance(coin)['available_balance']
                balances[i][coin] = balance
        self.balances = balances
        return balances

    def direction_calculator(self):        # tested
        """
            Description:
            This function is called to get the balances of coins in 
            two accounts.

            Functions used:
                None
            Parameters
            ----------
                None
            Return
            -------
                buy_account: int  (0 or 1 representing the two accounts)
        """
        if self.flow_direction > 0:
            buy_account = 0
            self.flow_direction -= 1
        elif self.flow_direction <= 0:
            buy_account = 1
            self.flow_direction += 1
        return buy_account


    def place_market_order(self, account_index, base, quote, order_side, quantity):     # tested
        """
            Description:
                If this is a buy order, everything is good. Just execute using the input quantity.
                However, if this is a sell order, the input quantity will be in base unit. Which means
                we must first convert the quantity into quote unit.
                Also, this is not an elegant market order function. It will place order across the spread 
                buy 10% which would sometime not completely filled.

            Functions used:
            local
            -----
                self.get_edge_prices()

            APIs
            -------
                API.place_order()
                API.cancel_order()

            Parameters
            ----------
                api: object (connect to api of a certain account)
                base: string (name of the base currency)
                quote: string (name of the quote currency)
                order_side: string ('buy' or 'sell')
                quantity: float (the amount of quote currency to sell)
            Return
            -------
                order_id: string 
        """
        api = self.API[account_index]
        edge_prices = self.get_edge_prices(base, quote)
        if order_side == 'buy':
            price = 1.1 * edge_prices['lowest_ask']
            quantity = round(quantity, self.precision[quote])
            order_id = api.place_order(base, quote, order_side, price, quantity)
        elif order_side == 'sell':
            price = 0.9 * edge_prices['highest_bid']
            quantity = round(quantity/edge_prices['highest_bid'], self.precision[quote])
            order_id = api.place_order(base, quote, order_side, price, quantity)
        time.sleep(3)
        api.cancel_order(base, quote, order_id, order_side)
        return order_id

    def place_limit_order(self, account_index, order_side, price, quantity):   # tested
        """
            Description:
                This function place a limit order given account
                and order details.

            Functions used:
            APIs
            -------
                API.place_order()

            Parameters
            ----------
                account_index: int
                order_side: buy or sell
                price: float
                quantity: float
            Return
            -------
                None
        """
        api = self.API[account_index]
        order_id = api.place_order(
            self.base_currency, 
            self.quote_currency, 
            order_side, 
            price, 
            quantity)
        self.order_info.append({
            'id': order_id, 
            'side': order_side, 
            'account': account_index})

    def cancel_existing_orders(self):    
        """
            Description:
                This function is to cancel orders placed
                by ideal events and rebalance events.

            Functions used:
            APIs
            -------
                API.cancel_order()

            Parameters
            ----------
                None
            Return
            -------
                None
        """
        while self.order_info:
            api = self.API[self.order_info[0]['account']]
            api.cancel_order(
                self.base_currency, 
                self.quote_currency, 
                self.order_info[0]['id'], 
                self.order_info[0]['side'])
            self.order_info.pop(0)
                
    def get_edge_prices(self, base, quote):    # tested
        """
            Description:
                Get lowest ask and highest bid of a currency pair.

            Functions used:
            APIs
            -------
                API.get_orderbook()

            Parameters
            ----------
                base: string (name of the base currency)
                quote: string (name of the quote currency)
            Return
            -------
                order_id: dict
                    {
                        'lowest_ask': float,
                        'highest_bid': float
                    }
        """
        edge_orders = self.API[0].get_orderbook(base, quote, 1)
        la = edge_orders['asks'][0]['price']
        hb = edge_orders['bids'][0]['price']
        return {'lowest_ask': la, 'highest_bid': hb}

    def get_last_price(self, base, quote):     # tested
        """
            Description:
                Get the last traded price of the quote-base pair.

            Functions used:
            Local
            -------
                self.get_last_price()
            Parameters
            ----------
                base: str
                quote: str
            Return
            -------
                price: float
        """
        last_trade = self.API[0].get_market_trade_history(base, quote, 1)[0]
        return last_trade['price']

    def update_quote_target(self):    # tested
        """
            Description:
                Since the usd price changes with time and out target
                volume is set in USD. We need to actively update
                quote volume target. This function recalculate the amount
                we have traded in term of USD and recalculate the quote
                left to trade for the time period. The usd price of the quote
                currency is calculated from the quote to base and base to usdt
                price on the corresponding exchange. This is not accurate but
                coinmarket cap is switching their API soon I do not want 
                that influence the performance of the bot.

            Functions used:
            Local
            -------
                self.get_last_price()
            Parameters
            ----------
                None
            Return
            -------
                None
        """
        quote_to_base = self.get_last_price(self.base_currency, self.quote_currency)
        base_to_usdt = self.get_last_price('usdt', self.base_currency)
        price = base_to_usdt * quote_to_base
        self.quote_target = (self.usd_target - self.quote_volume_traded * price) / price

    def sleep_time_randomizer(self):
        """
            Description:
                This function is to change frequency for a certain period of time.
                This is to introduce another level of randomness to the trading
                parttern. The sleep time is guaranteed to average to average_trade_interval
                in the long run.

            Parameters
            ----------
                None
            Return
            -------
                sleep: float
        """
        r = random.random()
        if r < self.average_trade_interval/self.frequency_change_interval:
            self.sleep_time = random.expovariate(1/self.average_trade_interval)
            print('Trading frequency switched to %s seconds per trade.' % self.sleep_time)
        sleep = self.sleep_time*2*random.random()
        return sleep

    def update_endtime(self):
        """
            This function is used to update the end time in trade session logger
            upon termination of the bot
        """
        end_timestr = datetime.datetime.fromtimestamp(int(time.time())).strftime('%Y-%m-%d %H:%M:%S')
        time_report = 'The bot with session id %s has stopped at %s .' % (self.session_id ,end_timestr)
        self.send_slack_message(time_report)
        self.collection.find_one_and_update({'_id': self.session_id}, {'$set': {'end_time': end_timestr}})

    def send_slack_message(self, text):
        """
            Report to slack
            Parameters
            ----------
                text: string
            Return
            ------
                None
        """
        print(slk.send_message_to_slack(self.slack_webhook, text))