import yaml
import time
# from multiprocessing import Process
from kucoin.client import Client
from kucoin import exceptions
import datetime
import decimal
import random
from random import randint
import logging

logging.basicConfig(filename='market_maker.log', level=logging.DEBUG)


class MarketMaker:
    def __init__(self):
        with open("config.yaml", "r") as stream:
            try:
                configs = (yaml.load(stream))
            except yaml.YAMLError as exception:
                print(exception)

        # ToDo randomize between 6-7 accounts
        self.FIRST_API_KEY = configs["client_one"]["KC_API_KEY"]
        self.FIRST_API_SECRET = configs["client_one"]["KC_API_SECRET"]
        self.SECOND_API_KEY = configs["client_two"]["KC_API_KEY"]
        self.SECOND_API_SECRET = configs["client_two"]["KC_API_SECRET"]
        self.client_one = Client(self.FIRST_API_KEY, self.FIRST_API_SECRET)
        self.client_two = Client(self.SECOND_API_KEY, self.SECOND_API_SECRET)

    def get_float_number(self, number):
        ctx = decimal.Context()
        ctx.prec = 20
        new_number = ctx.create_decimal(repr(number))
        return format(new_number, 'f')

    def get_user_input(self):
        details = {'symbol': None, 'price': None, 'amount': None}
        print("--- Order Details ---")
        print("\nEnter Symbol (e.g. J8T-BTC): ")
        details['symbol'] = input()
        # ToDo Validate Symbol
        print("\nEnter Comma Seperated Amount Range (e.g. 1,9): ")
        details['amount_range'] = input()
        # ToDo Validate integer amount range
        # ToDo validate second number > first number
        return details

    def fetch_balance(self, pair, buy_client, sell_client):
        sell_currency = pair.split('-')[0]
        buy_currency = pair.split('-')[1]
        sell_balance = sell_client.get_coin_balance(sell_currency)
        buy_balance = buy_client.get_coin_balance(buy_currency)
        print("\nFetched Balance on Seller Account is ")
        print(sell_balance)
        print("\nFetched Balance on Buyer Account is ")
        print(buy_balance)

        balance = {'sell_balance': sell_balance['balance'],
                   'buy_balance': buy_balance['balance']}
        return balance

    def calculate_price(self, pair):
        print("\nFetching Order Book from API")
        orders = self.client_one.get_order_book(pair)
        sell_price = orders['SELL'][0][0]
        buy_price = orders['BUY'][0][0]
        lowest_ask = self.get_float_number(sell_price)
        highest_bid = self.get_float_number(buy_price)
        decimal_places = 0
        currency = pair.split('-')[1]

        if currency == 'ETH':
            decimal_places = 7
        else:
            decimal_places = 8

        print("\nLowest Ask")
        print(lowest_ask + " (" + str(sell_price) + ")")
        print("\nHighest Bid")
        print(highest_bid + " (" + str(buy_price) + ")")

        highest_bid = float(highest_bid)
        lowest_ask = float(lowest_ask)
        difference = (lowest_ask - highest_bid)
        difference = abs(difference)
        half_difference = (difference / 2)
        price = (highest_bid + half_difference)
        price = round(price, decimal_places)

        if any([price < highest_bid, price > lowest_ask]):
            print("\nOrder Can not be placed due to narrow spread! Calculated Price: " + str(price))
            logging.debug("Order Can not be placed due to narrow spread! Calculated Price: " + str(price))
            return False
        else:
            price = self.get_float_number(price)
            print("\nCalculated Buy/Sell Price: " + price)
            # Returned Price is String because, if we convert it to int/float
            # it again becomes sceintific notation.
            return price

    def get_random_amount_from_range(self, amount_range):
        lower_range = amount_range.split(',')[0]
        higher_range = amount_range.split(',')[1]
        amount = format(random.uniform(int(lower_range),
                                       int(higher_range)), '.4f')
        print("\nRandom quantity for this attempt is " + amount)
        return float(amount)

    def set_random_client(self):
        first_client = random.choice([1, 2])
        if first_client == 1:
            return [self.client_one, self.client_two]
        else:
            return [self.client_two, self.client_one]

    def validate_balance(self, price, amount, balance):
        buy_balance = balance['buy_balance']
        sell_quantity = balance['sell_balance']
        buy_volume = float(price) * amount
        print("\nBuy Balance is " + str(buy_balance))
        print("\nAvailable Sell Quantity is " + str(sell_quantity))
        print("\nTrade Value (price x amount) is " + str(buy_volume))

        if buy_balance > buy_volume and sell_quantity > amount:
            return True
        else:
            print("\nERROR! Insufficient Balance.")
            logging.debug("ERROR! Insufficient Balance.")
            return False

    def submit_buy_order(self, client, symbol, price, amount):
        print("\nPlacing Buy Order Transaction")
        try:
            print("\n----------")
            logging.debug("----------")
            time = datetime.datetime.now()

            print("Submitting Buy Order with Symbol: " + symbol +
                  ", Price: " + price + ", Amount: " + amount +
                  " at " + str(time))
            logging.debug("Submitting Buy Order with Symbol: " + symbol +
                          ", Price: " + price + ", Amount: " + amount +
                          " at " + str(time))

            transaction = client.create_buy_order(symbol,
                                                  price,
                                                  amount)
            print(transaction)
            logging.debug(transaction)
            # ToDo check both orders should match
        except exceptions.KucoinAPIException as e:
            print("\nERROR! Something Went Wrong")
            print(e)
            logging.debug("ERROR! Something Went Wrong!")
            logging.debug(e)

    def submit_sell_order(self, client, symbol, price, amount):
        print("\nPlacing Sell Order Transaction")
        try:
            print("\n----------")
            logging.debug("----------")
            time = datetime.datetime.now()

            print("Submitting Sell Order with Symbol: " + symbol +
                  ", Price: " + price + ", Amount: " + amount +
                  " at " + str(time))
            logging.debug("Submitting Sell Order with Symbol: " + symbol +
                          ", Price: " + price + ", Amount: " + amount +
                          " at " + str(time))

            transaction = client.create_sell_order(symbol,
                                                   price,
                                                   amount)
            print(transaction)
            logging.debug(transaction)
        except exceptions.KucoinAPIException as e:
            print("\nERROR! Something Went Wrong")
            print(e)
            logging.debug("ERROR! Something Went Wrong!")
            logging.debug(e)

    def cancel_pending_orders(self, pair, buy_client, sell_client):
        sell_currency = pair.split('-')[0]
        buy_currency = pair.split('-')[1]
        # cancel buyer orders
        active_orders = buy_client.get_active_orders(buy_currency)
        if active_orders['SELL'] or active_orders['BUY']:
            print("\nActive Order(s) found in Buy Client. Canelling them.")
            logging.debug("Active Order(s) found. Cancelling them.")
            buy_client.cancel_all_orders(buy_currency)
        # cancel seller orders
        active_orders = sell_client.get_active_orders(sell_currency)
        if active_orders['SELL'] or active_orders['BUY']:
            print("\nActive Order(s) fund in Sell Client. Canelling them.")
            logging.debug("Active Orders(s) found. Cancelling them.")
            sell_client.cancel_all_orders(sell_currency)

    def submit_sell_first(self, details):
        logging.debug("Submitting Sell First")
        print("\nSubmitting Sell First")

        client = self.set_random_client()
        buy_client = client[0]
        sell_client = client[1]
        amount = self.get_random_amount_from_range(details['amount_range'])
        logging.debug("Buy Client ")
        logging.debug(buy_client)
        logging.debug("Sell Client ")
        logging.debug(sell_client)

        pair = details['symbol']
        balance = self.fetch_balance(pair, buy_client, sell_client)
        price = self.calculate_price(pair)
        sufficient_balance = self.validate_balance(price, amount, balance)

        if price is not False and sufficient_balance is True:
            self.submit_sell_order(sell_client, details['symbol'],
                                   price, amount)
            self.submit_buy_order(buy_client, details['symbol'],
                                  price, amount)
            self.cancel_pending_orders(pair, buy_client, sell_client)

    def submit_buy_first(self, details):
        logging.debug("Submitting Buy First")
        print("\nSubmitting Buy First")

        client = self.set_random_client()
        buy_client = client[0]
        sell_client = client[1]
        amount = self.get_random_amount_from_range(details['amount_range'])
        logging.debug("Buy Client ")
        logging.debug(buy_client)
        logging.debug("Sell Client ")
        logging.debug(sell_client)

        pair = details['symbol']
        balance = self.fetch_balance(pair, buy_client, sell_client)
        price = self.calculate_price(pair)
        sufficient_balance = self.validate_balance(price, amount, balance)

        if price is not False and sufficient_balance is True:
            self.submit_buy_order(buy_client, details['symbol'],
                                  price, amount)
            self.submit_sell_order(sell_client, details['symbol'],
                                   price, amount)
            self.cancel_pending_orders(pair, buy_client, sell_client)

    def place_both_orders(self, details):
        # Both order placement WITHOUT MultiProcessing
        if random.choice((True, False)):
            self.submit_buy_first(details)
        else:
            self.submit_sell_first(details)

    def schedule_both_orders(self):
        # Schedule placement of both orders at random interval
        # Get details and pair only once
        # price is fetched each time
        logging.debug("**")
        logging.debug("*********************************************************")
        logging.debug("**")
        logging.debug("STARTED AT " + time.ctime())
        details = self.get_user_input()
        strtime = time.time()

        print("\nEnter Comma Seperated Time Interval Range in Seconds: (e.g. 10, 30)")
        time_range = input()
        # ToDo validate integer input
        # ToDo validate second number > first number

        count = 0
        while count < 10:
            lower_time_range = time_range.split(',')[0]
            higher_time_range = time_range.split(',')[1]
            interval = randint(int(lower_time_range), int(higher_time_range))

            print("==========================")
            print("\nAttempt " + str(count))
            logging.debug("===============================")
            logging.debug("Attempt " + str(count))

            self.place_both_orders(details)
            print("\nSleeping for " + str(interval) + " seconds.")
            logging.debug("Sleeping for " + str(interval) + " seconds.")
            time.sleep(float(interval) - ((time.time() - strtime) %
                                          float(interval)))
            count += 1


def main():
    print("--- KuCoin API --- ")

    market_maker = MarketMaker()
    market_maker.schedule_both_orders()

main()
