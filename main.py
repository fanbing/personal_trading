"""Main File"""
import sys
import traceback
# import time
import yaml
import utils.check_inputs as check
from algos.iceberg.main import Iceberg
from algos.twap.main import Twap
from algos.dynamic_wall.main import Dynamic_wall
from algos.vexplode.main import Vexplode
from utils.db import Database

SUPPORTED_ALGOS = ["ICEBERG", "TWAP", "DYNAMIC_WALL", "VEXPLODE"]


def main():
    """Main Function"""
    try:
        with open('config.yaml', 'r') as config_file:
            # init
            data = (yaml.load(config_file))
            db_credentials = data["CREDENTIALS"]["MONGO"]
            init_db = Database(data, db_credentials)
            db = init_db.get_db()
            session_id = init_db.create_session()
            algo_name = data["CHOSEN_ALGO"]
            input_check = False

            if algo_name in SUPPORTED_ALGOS:
                if algo_name == "ICEBERG":
                    input_check = check.check_iceberg_inputs(data, session_id)
                    chosen_exchange = data["ICEBERG"]["EXCHANGE_NAME"]
                    algo = Iceberg(data["ICEBERG"], data["CREDENTIALS"][chosen_exchange], db, session_id)
                elif algo_name == 'TWAP':
                    input_check = check.check_twap_inputs(data, session_id)
                    chosen_exchange = data['TWAP']['EXCHANGE_NAME']
                    algo = Twap(data['TWAP'], data['CREDENTIALS'][chosen_exchange], db, session_id)
                elif algo_name == 'DYNAMIC_WALL':
                    input_check = check.check_dynamic_wall_inputs(data, session_id)
                    chosen_exchange = data['DYNAMIC_WALL']['EXCHANGE_NAME']
                    algo = Dynamic_wall(data['DYNAMIC_WALL'], data['CREDENTIALS'][chosen_exchange], db, session_id)
                elif algo_name == 'VEXPLODE':
                    input_check = check.check_vexplode_inputs(data, session_id)
                    algo = Vexplode(data['VEXPLODE'], db, session_id)
                # add other algos below
            else:
                raise ValueError('The algorithm is not yet supported')
            if input_check:
                algo.run()  # all algos should have the run function
            else:
                raise ValueError('Incorrect inputs for given algorithm.')

    except Exception as err:
        print("STARTUP ERROR: " + str(err))
        print(traceback.format_exc())
        sys.exit()

main()
